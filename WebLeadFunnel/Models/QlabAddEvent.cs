﻿namespace WebLeadFunnel.Models
{
    public class QlabAddEvent
    {
        public string AddType { get; set; }
        public string DomicileType { get; set; }
        public string DomicileSize { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string OriginZip { get; set; }
        public string DestinationZip { get; set; }
        public string MoveDate { get; set; }
        public string MoveType { get; set; }
        public string OriginAddress { get; set; }
        public string OriginCity { get; set; }
        public string OriginState { get; set; }
        public string DestinationAddress { get; set; }
        public string DestinationCity { get; set; }
        public string DestinationState { get; set; }
        public string AppointmentDatetime { get; set; }
        public string Source { get; set; }
    }
}
