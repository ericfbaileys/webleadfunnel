﻿using Microsoft.ApplicationInsights;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System;
using System.Text;
using WebLeadFunnel.Models;

namespace WebLeadFunnel.ServiceBus
{
    public class ServiceBusExecutor
    {
        private const string ConnectionString =
            "Endpoint=sb://baileysqlabadder.servicebus.windows.net/;SharedAccessKeyName=WebFunnelAccessKey;SharedAccessKey=sPBjMX1K64BW162NkEF0ascsrLDYcjtXKFa2CIbvZXE=";

        private const string ServiceBusQueueName = "qlabadditems";
        private readonly IQueueClient _serviceBusClient;

        private readonly TelemetryClient _telemetryClient;

        public ServiceBusExecutor(TelemetryClient telemetryClient)
        {
            _serviceBusClient = new QueueClient(ConnectionString, ServiceBusQueueName);
            _telemetryClient = telemetryClient;
        }

        public async void AddLeadToAdderQueue(FunnelContactDetail contactDetail)
        {
            // Convert to QlabAddEvent
            QlabAddEvent qlabAddEvent = new QlabAddEvent
            {
                AddType = "Lead",
                DomicileType = contactDetail.Type,
                DomicileSize = contactDetail.Steps.Split(',')[1],
                Name = contactDetail.Name,
                Phone = contactDetail.Phone,
                Email = contactDetail.Email,
                OriginZip = contactDetail.OriginZip.HasValue ? contactDetail.OriginZip.Value.ToString() : string.Empty,
                DestinationZip = contactDetail.DestinationZip.HasValue
                    ? contactDetail.DestinationZip.Value.ToString()
                    : string.Empty,
                MoveDate = contactDetail.MoveDate.HasValue
                    ? contactDetail.MoveDate.Value.ToShortDateString()
                    : string.Empty,
                MoveType = contactDetail.MoveType,
                OriginAddress = contactDetail.OriginAddress,
                OriginCity = contactDetail.OriginCity,
                OriginState = contactDetail.OriginState,
                DestinationAddress = contactDetail.DestinationAddress,
                DestinationCity = contactDetail.DestinationCity,
                DestinationState = contactDetail.DestinationState,
                Source = contactDetail.Source
            };

            // Send event to the queue
            try
            {
                // Add JSON to message
                Message adderMessage = new Message
                {
                    Body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(qlabAddEvent))
                };
                await _serviceBusClient.ScheduleMessageAsync(adderMessage, DateTimeOffset.UtcNow.AddSeconds(10));
                _telemetryClient.TrackEvent("SentInfoToQlabAdderQueue");
            }
            catch (Exception ex)
            {
                _telemetryClient.TrackException(ex);
            }
        }

        public async void AddOpportunityToAdderQueue(ContactInfoVM contactDetail)
        {
            // Convert datetime to include timezone info
            var apptDateTimeUtc = contactDetail.AppointmentDateTime.Value.ToUniversalTime();
            var mstTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
            var correctedApptDateTime = TimeZoneInfo.ConvertTime(apptDateTimeUtc, mstTimeZone);

            // Convert to QlabAddEvent
            QlabAddEvent qlabAddEvent = new QlabAddEvent
            {
                AddType = "Opportunity",
                DomicileType = contactDetail.MoveType,
                DomicileSize = contactDetail.Steps.Split(',')[1],
                Name = contactDetail.Name,
                Phone = contactDetail.Phone,
                Email = contactDetail.Email,
                OriginZip = contactDetail.OriginZip,
                DestinationZip = contactDetail.DestZip,
                MoveDate = contactDetail.MoveDate.ToString(),
                MoveType = contactDetail.MoveDistanceType,
                OriginAddress = contactDetail.OriginAddress,
                OriginCity = contactDetail.OriginCity,
                OriginState = contactDetail.OriginState,
                DestinationAddress = contactDetail.DestinationAddress,
                DestinationCity = contactDetail.DestinationCity,
                DestinationState = contactDetail.DestinationState,
                Source = !string.IsNullOrEmpty(contactDetail.Source) ? contactDetail.Source : "Web",
                AppointmentDatetime = correctedApptDateTime.ToString("yyyy-MM-ddTHH:mm:sszzz")
            };

            // Send event to the queue
            try
            {
                // Add JSON to message
                Message adderMessage = new Message
                {
                    Body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(qlabAddEvent))
                };
                await _serviceBusClient.ScheduleMessageAsync(adderMessage, DateTimeOffset.UtcNow.AddMinutes(10));
                _telemetryClient.TrackTrace($"SendOpportuntyJson: {JsonConvert.SerializeObject(qlabAddEvent)}");
                _telemetryClient.TrackEvent("SentInfoToQlabAdderQueue");
            }
            catch (Exception ex)
            {
                _telemetryClient.TrackException(ex);
            }
        }
    }
}
