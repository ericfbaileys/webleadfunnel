﻿using System.Text.Json.Serialization;

namespace WebLeadFunnel.Models
{
    public class YemboSendgridTokens
    {
        [JsonPropertyName("name")]
        public string FirstName { get; set; }
    }
}
