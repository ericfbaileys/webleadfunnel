﻿using System.Collections.Generic;
using System.Linq;

namespace WebLeadFunnel.Data
{
    public sealed class TokenStore
    {
        public static TokenStore Instance { get; } = new TokenStore();

        TokenStore() { }

        private readonly List<Token> _tokens = new List<Token>();

        public bool HasTokens => _tokens.Any();

        public Token GetCurrentToken()
        {
            // Get last available token
            return _tokens.OrderBy(x => x.Expiration).Last();
        }

        public void StoreNewToken(Token newToken)
        {
            _tokens.Add(newToken);
        }
    }
}
