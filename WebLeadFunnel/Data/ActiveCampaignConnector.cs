﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Internal;
using RestSharp;
using WebLeadFunnel.Models;

namespace WebLeadFunnel.Data
{
    public class ActiveCampaignConnector
    {
        private readonly string _baseApiUrl;
        private readonly string _apiUrl;

        /// <summary>
        /// Creates a new ActiveCampaign Connector using an API key
        /// </summary>
        /// <param name="baseUrl">Base URL for enviroment</param>
        /// <param name="apiKey">API key</param>
        public ActiveCampaignConnector(string baseUrl, string apiKey)
        {
            _baseApiUrl = baseUrl;
            _apiUrl = "/admin/api.php?api_output=json&api_key=" + apiKey;
        }

        public void AddNewContactToList(ContactInfoVM userData, int listId)
        {
            var client = new RestClient(_baseApiUrl);
            var request = new RestRequest(_apiUrl + "&api_action=contact_add", Method.POST);

            // Add options to header
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            // Add in parameter data
            request.AddParameter("email", userData.Email);
            string[] nameStrings = userData.Name.Split(' ');
            request.AddParameter("first_name", nameStrings?[0]);
            request.AddParameter("last_name",  nameStrings != null ? string.Join(" ", nameStrings.Skip(1).ToList()) : " ");
            request.AddParameter("phone", userData.Phone);
            request.AddParameter("field[%ESTIMATE_DATE%,0]", userData.AppointmentDateTime.Value.Date.ToString("d"));
            request.AddParameter("field[%ESTIMATE_TIME%],0", userData.AppointmentDateTime.Value.ToString("h:mm tt"));
            request.AddParameter("field[%SALESMAN_NAME%,0]", userData.EstimatorName);
            request.AddParameter("field[%REQUESTED_MOVE_DATE%],0", userData.MoveDate.Value.ToString("d"));
            request.AddParameter($"p[{listId}]", listId); // Add contact to list
            request.AddParameter($"status[{listId}]", 1); // 1: active, 2: unsubscribed

            // Set instant responder
            request.AddParameter($"instantresponders[{listId}]", 1);

            var response = client.ExecuteAsync(request);
            if (response.Result.StatusCode == HttpStatusCode.OK)
            {
                string temp = response.Result.Content;
            }
        }
    }
}
