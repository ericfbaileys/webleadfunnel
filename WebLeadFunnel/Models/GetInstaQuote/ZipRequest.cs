﻿namespace WebLeadFunnel.Models.GetInstaQuote
{
    public class ZipRequest
    {
        public string OrigZipHouse { get; set; }
        public string DestZipHouse { get; set; }
        public string OrigZipApt { get; set; }
        public string DestZipApt { get; set; }
        // Helper properties
        public string OriginZip => !string.IsNullOrEmpty(OrigZipHouse) ? OrigZipHouse : OrigZipApt;
        public string DestZip => !string.IsNullOrEmpty(DestZipHouse) ? DestZipHouse : DestZipApt;

        public ZipRequest()
        {
        }
    }
}
