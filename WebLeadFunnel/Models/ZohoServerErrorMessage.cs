﻿using System.Text.Json.Serialization;

namespace WebLeadFunnel.Models
{
    public class ZohoServerErrorMessage
    {
        [JsonPropertyName("errorCode")]
        public string ErrorCode { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }
    }
}
