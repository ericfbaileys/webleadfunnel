﻿namespace WebLeadFunnel.Models.GetInstaQuote
{
    public class QuoteRequest
    {
        public string Brand { get; } = "AVL";
        public string OriginStateCode { get; set; }
        public string OriginZip { get; set; }
        public string DestinationStateCode { get; set; }
        public string DestinationZip { get; set; }
        public string MoveDate { get; set; }
        public string HomeType { get; set; }
        public string HomeSize { get; set; }
        public string ContactName => string.Empty;
        public string ContactEmail => string.Empty;
        public string ContactPhone => string.Empty;

        public QuoteRequest()
        {
        }
    }
}
