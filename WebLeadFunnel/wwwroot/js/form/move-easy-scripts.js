var api_url = "https://baileysallied.moveeasy.com/move/iframe_inventory";
var div_container_id = "moveeasycontrol";
var frm_btn_submit = "";
var form_page_slug = "localmove";
var thanks_page_slug = "thanks";
var form_input_selector = "";
var parent_form_id = "";
var parent_form_class = "";
var field_name = "";
var current_page_url = window.location.href;
var pattern = new RegExp("/^(http://|https://)/");
current_page_url = current_page_url.replace(pattern, "");
current_page_string = current_page_url.split("/");
var current_page_new = "";
for (var j = 0; j < current_page_string.length; j++) {
    if (current_page_string[j] != "" && current_page_string[j].indexOf("?") == -1) {
        current_page_new = current_page_string[j];
    }
}
current_page = current_page_new;
if (current_page == form_page_slug) {
    document.addEventListener('DOMContentLoaded', function (event) {
        function parseDateString(dateArg) {
            if (dateArg.indexOf(' ') > 0) {
                dateArg = dateArg.split(' ');
                dateArg = dateArg[0];
            }
            dateString = dateArg;
            var date = new Date(dateString);
            var regexP = '/^(d{1,2})/(d{1,2})/(d{4})$/';
            var regexP3 = '/^(d{1,2})-(d{1,2})-(d{4})$/';
            var pattern1 = new RegExp(regexP, 'g');
            var pattern3 = new RegExp(regexP3, 'g');
            if (dateString.match(pattern1) && date == 'Invalid Date') {
                var dateParts = dateString.split('/');
                var string_date = dateParts[2] + '-' + (dateParts[1]) + '-' + dateParts[0];
            } else if (dateString.match(pattern3) && date == 'Invalid Date') {
                var dateParts = dateString.split('-');
                var string_date = dateParts[2] + '-' + (dateParts[1]) + '-' + dateParts[0];
            } else {
                var string_date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
            }
            return string_date;
        }

        function parsePhoneString(phoneArg) {
            var phoneString = phoneArg;
            var tel = phoneString;
            tel = tel.replace(/D+/g, '');
            phoneString = tel;
            return phoneString;
        }

        function findParentForm(elem) {
            var parent = elem.parentNode;
            if (parent && parent.tagName != 'FORM') {
                parent = findParentForm(parent);
            }
            return parent;
        }

        function getParentForm(elem) {
            var parentForm = findParentForm(elem);
            if (parentForm) {
                if (parentForm.className == '' && parentForm.id == '') {
                    parentForm.className = 'reQuestForm';
                    parent_form_class = parentForm.className;
                } else {
                    parentForm.className = parentForm.className + ' moveeasyReQuestForm';
                    parent_form_id = parentForm.id;
                    parent_form_class = 'moveeasyReQuestForm';
                }
            }
        }
        if (document.querySelector('input[name=""]') != null) {
            getParentForm(document.querySelector('input[name=""]'));

            function formFunction() {
                var field_emailid = "";
                var field_phone = "";
                var field_movesize = "0";
                var field_movedate = "";
                var field_fromzip = "";
                var field_tozip = "";
                var form_fields_name_str = "";
                if (parent_form_id == "") {
                    var request_form = document.querySelector("." + parent_form_class + "");
                } else {
                    var request_form = document.querySelector("#" + parent_form_id + "");
                }
                for (var i = 0; i < request_form.elements.length; i++) {
                    form_fields_name_str = request_form.elements[i].name;
                    if (request_form.elements[i] != null && form_fields_name_str == field_name) {
                        localStorage.setItem("fname", request_form.elements[i].value);
                    }
                    if (request_form.elements[i] != null && form_fields_name_str == field_emailid) {
                        localStorage.setItem("emailid", request_form.elements[i].value);
                    }
                    if (request_form.elements[i] != null && form_fields_name_str == field_phone) {
                        localStorage.setItem("phone", parsePhoneString(request_form.elements[i].value));
                    }
                    if (request_form.elements[i] != null && form_fields_name_str == field_movesize) {
                        var movesz = request_form.elements[i].value;
                        localStorage.setItem("movesize", movesz);
                    }
                    if (request_form.elements[i] != null && form_fields_name_str == field_movedate) {
                        localStorage.setItem("movedate", parseDateString(request_form.elements[i].value));
                    }
                    if (request_form.elements[i] != null && form_fields_name_str == field_fromzip) {
                        localStorage.setItem("fromzip", request_form.elements[i].value);
                    }
                    if (request_form.elements[i] != null && form_fields_name_str == field_tozip) {
                        localStorage.setItem("tozip", request_form.elements[i].value);
                    }
                }
            }
            var submitHandler = null;
            if (document.querySelector("." + parent_form_class + " input[name=submit]")) {
                submitHandler = document.querySelector("." + parent_form_class + " input[name=submit]");
            } else if (document.querySelector("." + parent_form_class + " input[type=submit]")) {
                submitHandler = document.querySelector("." + parent_form_class + " input[type=submit]");
            } else if (document.querySelector("." + parent_form_class + " input[id=submit]")) {
                submitHandler = document.querySelector("." + parent_form_class + " input[id=submit]");
            } else if (document.querySelector("." + parent_form_class + " button[type=submit]")) {
                submitHandler = document.querySelector("." + parent_form_class + " button[type=submit]");
            } else {
                alert("Invalid Submit handler");
            }
            if (submitHandler) {
                submitHandler.addEventListener("click", function (e) {
                    if (parent_form_class != "" || parent_form_class != "") {
                        formFunction();
                    }
                });
            }
        }
    });
}
if (current_page == thanks_page_slug) {
    document.addEventListener('DOMContentLoaded', function (event) {
        var fname = localStorage.getItem('fname');
        var emailid = localStorage.getItem('emailid');
        var phone = localStorage.getItem('phone');
        var movesize = localStorage.getItem('movesize');
        var movedate = localStorage.getItem('movedate');
        var fromzip = localStorage.getItem('fromzip');
        var tozip = localStorage.getItem('tozip');
        var iframe_url = "" + api_url + "?name=" + fname + "&email=" + emailid + "&move_size=" + movesize + "&move_date=" + movedate + "&zip_from=" + fromzip + "&zip_to=" + tozip + "&phone=" + phone + "&preference=Email";
        if (fname != "" || emailid != "" || phone != "" || movesize != "" || movedate != "" || fromzip != "" || tozip != "") {
            iframe_url = iframe_url.replace(" ", "+");
            document.querySelector("div#moveeasycontrol").innerHTML = '<iframe src="' + iframe_url + '" style="width:1200px; height:1500px; min-width:320px;" frameborder="0"></iframe>'
        }
        var fname = localStorage.setItem('fname', '');
        var emailid = localStorage.setItem('emailid', '');
        var phone = localStorage.setItem('phone', '');
        var movesize = localStorage.setItem('movesize', '');
        var movedate = localStorage.setItem('movedate', '');
        var fromzip = localStorage.setItem('fromzip', '');
        var tozip = localStorage.setItem('tozip', '');
    });
}