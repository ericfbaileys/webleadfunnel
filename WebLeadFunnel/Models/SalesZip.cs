﻿namespace WebLeadFunnel.Models
{
    public class SalesZip
    {
        public int Id { get; set; }
        public int ZipCode { get; set; }

        public SalesZip()
        {
        }
    }
}
