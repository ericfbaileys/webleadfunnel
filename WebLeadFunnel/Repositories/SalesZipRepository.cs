﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using WebLeadFunnel.Models;
using System.Collections.Generic;
using Microsoft.ApplicationInsights;
using Microsoft.Data.SqlClient;

namespace WebLeadFunnel.Repositories
{
    public class SalesZipRepository
    {
        private readonly string _connectionString;
        private readonly TelemetryClient _telemetry;
        public SalesZipRepository(string connectionString, TelemetryClient telemetryClient)
        {
            _connectionString = connectionString;
            _telemetry = telemetryClient;

        }
        public IDbConnection Connection => new SqlConnection(_connectionString);

        /// <summary>
        /// Determine if a zipcode exists in the database
        /// </summary>
        /// <param name="zipCode">Zip Code</param>
        /// <returns>True of the Zip Code exists</returns>
        public async Task<bool> IsInServiceArea(string zipCode)
        {
            using (IDbConnection dbConnection = Connection)
            {
                try
                {
                    var result = await dbConnection.QueryAsync<int>("SELECT COUNT(*) FROM [dbo].[SalesZips] WHERE ZipCode = @Zip", new {Zip=zipCode});
                    return result.First() == 1;
                }
                catch (Exception ex)
                {
                    _telemetry.TrackException(ex);
                }
            }
            return false;
        }

        /// <summary>
        /// Determine if a zipcode exists in the database
        /// </summary>
        /// <param name="zipCode">Zip Code</param>
        /// <returns>True of the Zip Code exists</returns>
        public async Task<SalesPerson> SalesPersonForZip(string zipCode)
        {
            using (IDbConnection dbConnection = Connection)
            {
                try
                {
                    var result = await dbConnection.QueryAsync<SalesPerson>("SELECT SalesPersons.Id, SalesPersons.Name, SalesPersons.Location, SalesPersons.ScheduleDaysOut, SalesPersons.ScheduleHoursApart, SalesPersons.ScheduleMinutesApart, SalesPersons.CalendarId, SalesPersons.LocationCode FROM [dbo].[SalesPersons] inner join SalesZips ON SalesPersons.Id=SalesZips.SalesPerson_Id where SalesZips.ZipCode = @Zip", new { Zip = zipCode });
                    return result.First();
                }
                catch (Exception ex)
                {
                    _telemetry.TrackException(ex);
                }
            }
            return null;
        }

        /// <summary>
        /// Determine if a zipcode exists in the database
        /// </summary>
        /// <param name="zipCode">Zip Code</param>
        /// <returns>True of the Zip Code exists</returns>
        public async Task<SalesPerson> SalesPersonByName(string fullName)
        {
            using (IDbConnection dbConnection = Connection)
            {
                try
                {
                    var result = await dbConnection.QueryAsync<SalesPerson>("SELECT SalesPersons.Id, SalesPersons.Name, SalesPersons.Location, SalesPersons.ScheduleDaysOut, SalesPersons.ScheduleHoursApart, SalesPersons.ScheduleMinutesApart, SalesPersons.CalendarId, SalesPersons.LocationCode FROM [dbo].[SalesPersons] where SalesPersons.Name = @FullName", new { FullName = fullName });
                    return result.First();
                }
                catch (Exception ex)
                {
                    _telemetry.TrackException(ex);
                }
            }
            return null;
        }

        public async Task<List<SalesScheduleAvailibility>> GetAvailbilityForSalesPerson(string salesPersonId)
        {
            using (IDbConnection dbConnection = Connection)
            {
                try
                {
                    var result = await dbConnection.QueryAsync<SalesScheduleAvailibility>("Select * FROM [dbo].[SalesScheduleAvailibilities] where SalesPerson_Id = @SalesPerson", new { SalesPerson = salesPersonId});
                    return result.ToList();
                }
                catch (Exception ex)
                {
                    _telemetry.TrackException(ex);
                }
            }
            return null;
        }

        /// <summary>
        /// Store new contact info for potential and completed leads
        /// </summary>
        /// <param name="contactDetail">Contact Information</param>
        public async void StoreContactInfo(FunnelContactDetail contactDetail)
        {
            using (IDbConnection dbConnection = Connection)
            {
                try
                {
                    string query = "INSERT INTO [dbo].[FunnelContactDetails] ([CreatedAt],[Type],[MoveType],[ContactFormName],[Steps],[Name],[Phone],[Email],[OriginZip],[DestinationZip],[MoveDate],[OfficeCompanyName],[OfficeTimeframe],[OfficeTellAboutProject],[OriginAddress],[DestinationAddress],[OriginCity],[OriginState],[DestinationCity],[DestinationState],[SalesPersonName],[SalesPersonLocationCode], [PromoCode], [Source]) VALUES (@CreatedAt, @Type, @MoveType, @ContactFormName, @Steps, @Name, @Phone, @Email, @OriginZip, @DestinationZip, @MoveDate, @OfficeCompanyName, @OfficeTimeframe, @OfficeTellAboutProject, @OriginAddress, @DestinationAddress, @OriginCity, @OriginState, @DestinationCity, @DestinationState, @SalesPersonName, @SalesPersonLocationCode, @PromoCode, @Source)";
                    await dbConnection.ExecuteAsync(query, contactDetail);
                }
                catch (Exception ex)
                {
                    _telemetry.TrackException(ex);
                }
            }
        }

        public async void AddAppointmentToContactDetail(ContactInfoVM userData)
        {
            // Get the existing contact info
            using (IDbConnection dbConnection = Connection)
            {
                try
                {
                    var existingContact = await dbConnection.QueryFirstOrDefaultAsync<FunnelContactDetail>("Select * FROM [dbo].[FunnelContactDetails] where Email = @Email and Name = @Name and Phone = @Phone order by CreatedAt desc", new { Email = userData.Email, Name = userData.Name, Phone = userData.Phone});
                    if (existingContact != null)
                    {
                        string appointmentAddQuery =
                            "UPDATE [dbo].[FunnelContactDetails] set AppointmentDateTime = @AppointmentDateTime, AppointmentType = @AppointmentType, OriginAddress = @OriginAddress, DestinationAddress = @DestinationAddress where Id = @Id";
                        await dbConnection.ExecuteAsync(appointmentAddQuery, new { AppointmentDateTime = userData.AppointmentDateTime, AppointmentType = userData.ContactFormName, OriginAddress = userData.OriginAddress, DestinationAddress = userData.DestinationAddress, Id = existingContact.Id});
                    }
                }
                catch (Exception ex)
                {
                    _telemetry.TrackException(ex);
                }
            }

        }
    }
}
