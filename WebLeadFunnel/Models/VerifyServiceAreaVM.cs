﻿using System;

namespace WebLeadFunnel.Models
{
    public class VerifyServiceAreaVM
    {
        public string Steps { get; set; }
        public double MoveDistance { get; set; }
        public string OrigZipHouse { get; set; }
        public string DestZipHouse { get; set; }
        public DateTime HouseDate { get; set; }
        public string HouseName { get; set; }
        public string HousePhone { get; set; }
        public string HouseEmail { get; set; }

        public string PromoCode { get; set; }
        public string Source { get; set; }

        public string OrigZipApt { get; set; }
        public string DestZipApt { get; set; }
        public DateTime AptDate { get; set; }
        public string AptName { get; set; }
        public string AptPhone { get; set; }
        public string AptEmail { get; set; }
        // City/State from zip check
        public string OriginCity { get; set; }
        public string OriginState { get; set; }
        public string DestinationCity { get; set; }
        public string DestinationState { get; set; }

        // Helper properties
        public string OriginZip => Steps.Split(',')[0] == "House" ? OrigZipHouse : OrigZipApt;
        public string DestZip => Steps.Split(',')[0] == "House" ? DestZipHouse : DestZipApt;
        public DateTime MoveDate => Steps.Split(',')[0] == "House" ? HouseDate : AptDate;
        public string Name => Steps.Split(',')[0] == "House" ? HouseName : AptName;
        public string Phone => Steps.Split(',')[0] == "House" ? HousePhone : AptPhone;
        public string Email => Steps.Split(',')[0] == "House" ? HouseEmail : AptEmail;
        public string MoveDateFormatted => MoveDate.ToShortDateString();
        public string MoveType => Steps.Split(',')[0];
        public string MoveSize => Steps.Split(',')[1];

        public VerifyServiceAreaVM() { }
    }
}
