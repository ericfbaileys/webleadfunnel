﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Newtonsoft.Json;
using RestSharp;
using WebLeadFunnel.Models.GetInstaQuote;

namespace WebLeadFunnel.Data
{
    public class QlabExecutor
    {
        private readonly TelemetryClient _telemetry;
        private readonly string BaseUri = "https://openapi.sirva.com";
        private readonly QlabConsumer _consumer;

        public QlabExecutor(QlabConsumer consumer, TelemetryClient telemetryClient)
        {
            _consumer = consumer;
            _telemetry = telemetryClient;
        }

        public QuoteResult GetMoveQuote(QuoteRequest quoteRequest)
        {
            try
            {
                var client = new RestClient(BaseUri);
                var request = new RestRequest("/LMP/m7/GetInstaQuote/", Method.POST);

                // Add options to header
                request.AddHeader("Host", "openapi.sirva.com");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Accept", "application/json");
                // Add current access token for request
                request.AddHeader("Authorization",
                    _consumer.GetAccessToken().Result.TokenType + " " + _consumer.GetAccessToken().Result.AccessToken);

                request.AddJsonBody(quoteRequest);

                var response = client.ExecuteAsync(request);
                if (response.Result.StatusCode == HttpStatusCode.OK)
                {
                    if (!string.IsNullOrEmpty(response.Result.Content))
                    {
                        _telemetry.TrackTrace(response.Result.Content, SeverityLevel.Verbose);

                        return JsonConvert.DeserializeObject<QuoteResult>(response.Result.Content,
                            new JsonSerializerSettings()
                            {
                                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                                DateParseHandling = DateParseHandling.None
                            });
                    }
                }
                // If the HTTP code is anything else, return our error message
                return null;
            }
            catch (Exception ex)
            {
                _telemetry.TrackException(ex);
            }
            return null;
        }
    }
}
