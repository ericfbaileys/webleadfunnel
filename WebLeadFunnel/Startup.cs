﻿using System;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using BaileysNuget.ActiveCampaignApiService;
using BaileysNuget.SendgridService;
using Google.Apis.Http;
using Microsoft.ApplicationInsights.AspNetCore;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights.SnapshotCollector;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace WebLeadFunnel
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // The following line enables Application Insights telemetry collection.
            var aiOptions = new Microsoft.ApplicationInsights.AspNetCore.Extensions.ApplicationInsightsServiceOptions
            {
                EnableAdaptiveSampling = false
            };
            services.AddApplicationInsightsTelemetry(aiOptions);

            // Configure SnapshotCollector from application settings
            services.Configure<SnapshotCollectorConfiguration>(Configuration.GetSection(nameof(SnapshotCollectorConfiguration)));

            // Add SnapshotCollector telemetry processor.
            services.AddSingleton<ITelemetryProcessorFactory>(sp => new SnapshotCollectorTelemetryProcessorFactory(sp));

            services.AddHttpClient("ZohoDeskApi");

            // Keep default DateTime values in server timezone instead of UTC
            services.AddControllersWithViews(options => options.ModelBinderProviders.RemoveType<DateTimeModelBinderProvider>());

            // Enable gzip compression
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);
            services.AddResponseCompression(options =>
            {
                // Set the mime types to compress, use concat to add more to the default list
                options.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[]
                {
                    "image.svg+xml",
                    "application/xhtml+xml"
                });
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;
            });
            // App insights
            services.AddApplicationInsightsTelemetry(Configuration);
            // Enable caching with defaults
            services.AddResponseCaching();
            // Add framework services.
            services.AddMvc().AddRazorRuntimeCompilation().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0);

            // Allow the configuration to be used in controllers to access the connection string
            services.AddSingleton<IConfiguration>(Configuration);

            // Get KeyVault client to inject into services
            SecretClient keyvaultSecretClient = new SecretClient(new Uri(Environment.GetEnvironmentVariable("BaileysInternalVaultUri")), new DefaultAzureCredential());

            services.AddTransient(s => new SendgridEmailService(keyvaultSecretClient));

            services.AddScoped(x => new ActiveCampaignService(x.GetService<HttpClient>()));

            // Add EF6 database connection context
            //services.AddEntityFrameworkSqlServer().AddDbContextPool<QlabDbContext>(options=>options.UseSqlServer(Configuration.GetConnectionString("QlabDatabase")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            // Specify non-standard MIME types 
            var provider =
                new FileExtensionContentTypeProvider { Mappings = { [".webmanifest"] = "application/manifest+json", [".svg"] = "image/svg+xml" } };

            app.UseStaticFiles(
                new StaticFileOptions
                {
                    OnPrepareResponse = ctx =>
                    {
                        ctx.Context.Response.Headers.Append("Cache-Control", "public,max-age=6000");
                    },
                    ContentTypeProvider = provider
                });

            app.UseRouting();

            app.UseResponseCompression();
            app.UseResponseCaching();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }


        /// <summary>
        /// Factory class to create the Application Insights Exception Snapshot Collector
        /// </summary>
        private class SnapshotCollectorTelemetryProcessorFactory : ITelemetryProcessorFactory
        {
            private readonly IServiceProvider _serviceProvider;

            public SnapshotCollectorTelemetryProcessorFactory(IServiceProvider serviceProvider) =>
                _serviceProvider = serviceProvider;

            public ITelemetryProcessor Create(ITelemetryProcessor next)
            {
                var snapshotConfigurationOptions = _serviceProvider.GetService<IOptions<SnapshotCollectorConfiguration>>();
                return new SnapshotCollectorTelemetryProcessor(next, configuration: snapshotConfigurationOptions.Value);
            }
        }
    }
}
