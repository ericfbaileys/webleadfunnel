﻿using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using BaileysNuget.ActiveCampaignApiService;
using BaileysNuget.ActiveCampaignApiService.Models;
using BaileysNuget.SendgridService;
using BaileysNuget.SendgridService.Models;
using BaileysNuget.ZohoApiService;
using BaileysNuget.ZohoApiService.Models;
using BaileysNuget.ZohoTokenService;
using BinaryFog.NameParser;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Json;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using WebLeadFunnel.Data;
using WebLeadFunnel.Models;
using WebLeadFunnel.Models.GetInstaQuote;
using WebLeadFunnel.Repositories;
using WebLeadFunnel.ServiceBus;

namespace WebLeadFunnel.Controllers
{
    [ApiController]
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _hostingEnvironment;

        private readonly TelemetryClient _telemetry;
        private readonly string _connectionString;
        private readonly SalesZipRepository zipRepo;

        private SendgridEmailService _sendgridEmailService { get; }

        private readonly IHttpClientFactory _clientFactory;
        private readonly ActiveCampaignService _activeCampaignService;

        /// <summary>
        /// Allow the IConfiguration to be passed in to get the connection string
        /// </summary>
        /// <param name="config"></param>
        /// <param name="hostingEnv"></param>
        /// <param name="telemetryClient"></param>
        /// <param name="clientFactory"></param>
        /// <param name="sendgridEmailService"></param>
        /// <param name="activeCampaignService"></param>
        public HomeController(IConfiguration config, IWebHostEnvironment hostingEnv, TelemetryClient telemetryClient, IHttpClientFactory clientFactory, SendgridEmailService sendgridEmailService, ActiveCampaignService activeCampaignService)
        {
            _configuration = config;
            _hostingEnvironment = hostingEnv;

            _telemetry = telemetryClient;
            _clientFactory = clientFactory;

            _connectionString = config.GetConnectionString("QlabDatabase");
            // Get repo to make Zip Code lookups
            zipRepo = new SalesZipRepository(_connectionString, _telemetry);

            _sendgridEmailService = sendgridEmailService;
            _activeCampaignService = activeCampaignService;
        }

        [HttpGet]
        [Route("test")]
        public async Task<IActionResult> Test()
        {
            /*
            FunnelContactDetail contactDetail = new FunnelContactDetail();

            contactDetail.Steps = "Office/Industrial,50+ Employees,Confirm";
            contactDetail.Name = "Melinda Robertson";
            contactDetail.Phone = "719-725-1134";
            contactDetail.Type = "Office/Industrial";
            contactDetail.Email = "melinda.robertson@everty-group.com";
            contactDetail.OfficeCompanyName = "Tek Experts, Inc.";
            contactDetail.OfficeTimeframe = "Moving in January";
            contactDetail.OfficeTellAboutProject =
                "We have two building we are moving from and into a new building. We would like to Move out of one in Jan and the other at the end of Feb. We have workstations, equipment, files, boxes, hanging items, TV's and some furniture. ";


            await CreateZohoTicketForRfc(contactDetail);

            ContactCreationRequest contactCreationRequest = new ContactCreationRequest()
            {
                Contact = new CreateContactRequest()
                {
                    FirstName = "Testtt",
                    LastName = "httpclient",
                    Email = "test23234242343@test.com",
                }
            };
            var result = await _activeCampaignService.TryCreateNewContactAsync(contactCreationRequest);

            */

            DateTime tempp = DateTime.Now;
            var tempUTC = tempp.ToUniversalTime();
            var mstTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
            var correctedFullTime = TimeZoneInfo.ConvertTime(tempUTC, mstTimeZone);

            return Content(correctedFullTime.ToString("yyyy-MM-ddTHH:mm:sszzz"));

            //return View("Learn More");
        }


        [HttpGet]
        [Route("")]
        [ResponseCache(Duration = 86400)]
        public IActionResult Index(string promoCode)
        {
            ViewData["FormPageTitle"] = "Get Your Quote";
            ViewData["FormPageDescription"] = "Help us learn about your needs";

            //ViewData["PromoCode"] = promoCode;

            return View("Learn More");
        }

        [HttpGet]
        [Route("Home/LocalMove")]
        public IActionResult LocalMove()
        {
            // Not needed if we aren't using the templated version
            //ViewData["FormPageTitle"] = "Get Your Quote";
            //ViewData["FormPageDescription"] = "Help us learn about your needs";
            return View();
        }


        /// <summary>
        /// Checks to see if either Zip code is within our service areas and gets the estimator for later
        /// </summary>
        /// <param name="verifyServiceAreaModel">User submitted information</param>
        /// <returns>JSON with estimator info</returns>
        [HttpPost]
        [Route("Home/VerifyServiceArea")]
        public async Task<IActionResult> VerifyServiceArea(VerifyServiceAreaVM verifyServiceAreaModel)
        {
            if (verifyServiceAreaModel == null) return new BadRequestResult();
            // Build model for the result being send back to the front-end
            VerifyServiceResult verifyResult = new VerifyServiceResult { ResponseType = "neither" };

            // See which places are in our service area
            bool hasOrigZip = await zipRepo.IsInServiceArea(verifyServiceAreaModel.OriginZip);
            bool hasDestZip = await zipRepo.IsInServiceArea(verifyServiceAreaModel.DestZip);

            // Track zip codes that are checked
            Dictionary<string, string> zipsDictionary = new Dictionary<string, string>
            {
                {"OriginZip", verifyServiceAreaModel.OriginZip},
                {"DestinationZip", verifyServiceAreaModel.DestZip}
            };
            _telemetry.TrackEvent("Zipcode Lookup", zipsDictionary);
            if (hasOrigZip)
            { // Track when the origin is in our service area
                _telemetry.TrackEvent("OriginInServiceArea");
            }

            // Save off user's info for potential follow up
            FunnelContactDetail contactDetail = new FunnelContactDetail
            {
                CreatedAt = DateTime.Now,
                Steps = verifyServiceAreaModel.Steps,
                Type = verifyServiceAreaModel.Steps.Split(",")[0],
                MoveType = verifyServiceAreaModel.Steps.Split(",")[2],
                MoveDate = verifyServiceAreaModel.MoveDate,
                Name = verifyServiceAreaModel.Name.Trim(),
                Phone = verifyServiceAreaModel.Phone.Trim(),
                Email = verifyServiceAreaModel.Email.Replace(" ", string.Empty).Trim(),
                OriginCity = verifyServiceAreaModel.OriginCity,
                OriginState = verifyServiceAreaModel.OriginState,
                DestinationCity = verifyServiceAreaModel.DestinationCity,
                DestinationState = verifyServiceAreaModel.DestinationState,
                PromoCode = verifyServiceAreaModel.PromoCode,
                Source = verifyServiceAreaModel.Source
            };
            // Get zip codes to store in database
            int.TryParse(verifyServiceAreaModel.OriginZip, out var potentialOriginZip);
            contactDetail.OriginZip = potentialOriginZip;
            int.TryParse(verifyServiceAreaModel.DestZip, out var potentialDestZip);
            contactDetail.DestinationZip = potentialDestZip;

            // Add in Salesperson info
            if (contactDetail.OriginZip.HasValue)
            {
                if (zipRepo.IsInServiceArea(contactDetail.OriginZip.Value.ToString()).Result)
                {
                    SalesPerson estimator = zipRepo.SalesPersonForZip(contactDetail.OriginZip.Value.ToString()).Result;
                    contactDetail.SalesPersonName = estimator.Name;
                    contactDetail.SalesPersonLocationCode = estimator.LocationCode;
                }
            }

            // Check distance to determine ContactFormName
            contactDetail.ContactFormName = verifyServiceAreaModel.MoveDistance <= 50 ? "PotentialME" : "Potential";

            // Only store info in production
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME")))
            {
                // Store the contact info in the database
                SalesZipRepository repo = new SalesZipRepository(_connectionString, _telemetry);
                repo.StoreContactInfo(contactDetail);
                Dictionary<string, string> potentialContactDictionary = new Dictionary<string, string>
                {
                    {"Name", verifyServiceAreaModel.Name},
                    {"Email", verifyServiceAreaModel.Email}
                };
                _telemetry.TrackEvent("StoredPotentialContactInfo", potentialContactDictionary);

                // Send event to queue to be processed
                ServiceBusExecutor serviceBusExecutor = new ServiceBusExecutor(_telemetry);
                serviceBusExecutor.AddLeadToAdderQueue(contactDetail);
            }

            // Both exist in service area
            if (hasOrigZip && hasDestZip)
            {
                verifyResult.ResponseType = "both";
                SalesPerson estimator = await zipRepo.SalesPersonForZip(verifyServiceAreaModel.OriginZip);
                verifyResult.EstimatorName = estimator.Name;
                verifyResult.EstimatorLocation = estimator.Location;
                verifyResult.ScheduleDaysOut = estimator.ScheduleDaysOut;
            }

            // One zip exists in service area
            if (hasOrigZip || hasDestZip)
            {
                verifyResult.ResponseType = hasOrigZip ? "origin" : "destination";
                SalesPerson estimator = await zipRepo.SalesPersonForZip(hasOrigZip ? verifyServiceAreaModel.OriginZip : verifyServiceAreaModel.DestZip);
                verifyResult.EstimatorName = estimator.Name;
                verifyResult.EstimatorLocation = estimator.Location;
                verifyResult.ScheduleDaysOut = estimator.ScheduleDaysOut;
            }

            return Json(verifyResult);
        }

        /// <summary>
        /// Runs a ballpark estimation based on Allied's API
        /// </summary>
        /// <returns>HTML to display in estimator page</returns>
        [HttpPost]
        [Route("Home/Ballpark")]
        public IActionResult Ballpark(VerifyServiceAreaVM verifyServiceAreaModel)
        {
            // Call SIRVA Api
            var instaQuoteResult = GetBallparkQuoteJson(verifyServiceAreaModel);

            if (instaQuoteResult != null)
            {
                // Add quote values to telemetry
                try
                {
                    // Get geometric mean as average
                    double averagePrice = Math.Sqrt(double.Parse(instaQuoteResult.Average.HighPrice) *
                                                    double.Parse(instaQuoteResult.Average.LowPrice));
                    _telemetry.TrackMetric("AverageQuoteValue", averagePrice);
                }
                catch (Exception ex)
                {
                    _telemetry.TrackException(ex);
                }

                // Make sure we have valid results for the user
                if (instaQuoteResult.Result != null && instaQuoteResult.Result.Results != "False")
                {
                    // Check that every field came back from the quote request or consider it invalid
                    if (string.IsNullOrEmpty(instaQuoteResult.NotMuch.LowPrice) ||
                        string.IsNullOrEmpty(instaQuoteResult.NotMuch.HighPrice) ||
                        string.IsNullOrEmpty(instaQuoteResult.Average.LowPrice) ||
                        string.IsNullOrEmpty(instaQuoteResult.Average.HighPrice) ||
                        string.IsNullOrEmpty(instaQuoteResult.ALot.LowPrice))
                    {
                        // Track how often we don't get a value for a field
                        if (instaQuoteResult.NotMuch?.LowPrice == "N/A" ||
                            instaQuoteResult.NotMuch?.HighPrice == "N/A" ||
                            instaQuoteResult.Average?.LowPrice == "N/A" ||
                            instaQuoteResult.Average?.HighPrice == "N/A" ||
                            instaQuoteResult.ALot?.LowPrice == "N/A")
                        {
                            _telemetry.TrackEvent("Quote had N/A value(s)");
                        }

                        instaQuoteResult =
                            null; // This will trigger the view fall back to a graceful error for the user
                    }
                }
                else
                {
                    instaQuoteResult = null;
                }

            }
            return PartialView("BallparkView", instaQuoteResult);
        }

        /// <summary>
        /// Runs a local ballpark using Bailey's algorithm
        /// </summary>
        /// <returns>HTML to display in estimator page</returns>
        [HttpPost]
        [Route("Home/LocalBallpark")]
        public async Task<IActionResult> LocalBallpark(LocalMoveQuoteRequest localMoveQuoteRequest)
        {
            // Call Bailey's API
            var localQuoteResult = await GetLocalQuote(localMoveQuoteRequest);

            if (localQuoteResult != null)
            {
                // Add quote values to telemetry
                try
                {
                    if (!string.IsNullOrEmpty(localQuoteResult.Mid.Rate))
                    {
                        _telemetry.TrackMetric("AverageLocalQuoteValue",
                            double.Parse(localQuoteResult.Mid.Rate));
                    }
                }
                catch (Exception ex)
                {
                    _telemetry.TrackException(ex);
                }
                // Round the rates up to the nearest $25 increment
                localQuoteResult.High.Rate = decimal.Parse(localQuoteResult.High.Rate).RoundOffto25().ToString("N0");
                localQuoteResult.Mid.Rate = decimal.Parse(localQuoteResult.Mid.Rate).RoundOffto25().ToString("N0");
                localQuoteResult.Low.Rate = decimal.Parse(localQuoteResult.Low.Rate).RoundOffto25().ToString("N0");

                return PartialView("LocalBallparkView", localQuoteResult);
            }

            return Content("");
        }

        [HttpPost]
        [Route("Home/LocalBallparkJson")]
        public IActionResult LocalBallparkJson(LocalMoveQuoteRequest localMoveQuoteRequest)
        {
            var localQuoteResult = GetLocalQuote(localMoveQuoteRequest).Result;

            // Round the rates up to the nearest $25 increment
            localQuoteResult.High.Rate = decimal.Parse(localQuoteResult.High.Rate).RoundOffto25().ToString("N0");
            localQuoteResult.Mid.Rate = decimal.Parse(localQuoteResult.Mid.Rate).RoundOffto25().ToString("N0");
            localQuoteResult.Low.Rate = decimal.Parse(localQuoteResult.Low.Rate).RoundOffto25().ToString("N0");

            return Json(localQuoteResult);
        }

        private async Task<LocalQuoteResult> GetLocalQuote(LocalMoveQuoteRequest localMoveQuoteRequest)
        {
            string baileysBackendUrl;
            // Use different backend when in production
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME")))
            {
                baileysBackendUrl = "https://internal.baileysallied.com";
            }
            else
            {
                baileysBackendUrl = "https://localhost:44322";
            }

            // Create API call to backend for local quote
            var restClient = new RestClient(baileysBackendUrl);
            var request = new RestRequest("/BMS/Quoting/QuoteJson", Method.POST);

            request.AddJsonBody(localMoveQuoteRequest);

            var response = await restClient.ExecuteAsync(request);
            if (response.IsSuccessful)
            {
                // Parse the local quote info
                return JsonConvert.DeserializeObject<LocalQuoteResult>(response.Content);
            }

            return null;
        }

        /// <summary>
        /// Runs a ballpark estimation based on Allied's API
        /// </summary>
        /// <returns>JSON from SIRVA api</returns>
        [HttpPost]
        [Route("Home/BallparkJson")]
        public IActionResult BallparkJson(VerifyServiceAreaVM verifyServiceAreaModel)
        {
            var quoteResult = GetBallparkQuoteJson(verifyServiceAreaModel);
            return Json(quoteResult);
        }

        private QuoteResult GetBallparkQuoteJson(VerifyServiceAreaVM verifyServiceAreaModel)
        {
            // Track the base info used for the ballpark lookup
            Dictionary<string, string> ballparkDictionary = new Dictionary<string, string>
            {
                {"MoveDate", verifyServiceAreaModel.MoveDateFormatted},
                {"HomeType", verifyServiceAreaModel.MoveType},
                {"HomeSize", verifyServiceAreaModel.MoveSize},
                {"OriginStateCode", verifyServiceAreaModel.OriginState},
                {"DestinationStateCode", verifyServiceAreaModel.DestinationState}
            };
            _telemetry.TrackEvent("BallparkLookup", ballparkDictionary);

            // Check zips for length to make sure zips starting with zero get quoted properly
            string originZip = verifyServiceAreaModel.OriginZip;
            if (verifyServiceAreaModel.OriginZip.Length == 4)
            {
                originZip = "0" + verifyServiceAreaModel.OriginZip;
            }

            string destinationZip = verifyServiceAreaModel.DestZip;
            if (verifyServiceAreaModel.DestZip.Length == 4)
            {
                destinationZip = "0" + verifyServiceAreaModel.DestZip;
            }

            QuoteRequest quoteRequest = new QuoteRequest
            {
                OriginZip = originZip,
                DestinationZip = destinationZip,
                MoveDate = verifyServiceAreaModel.MoveDateFormatted,
                HomeType = verifyServiceAreaModel.MoveType,
                HomeSize = verifyServiceAreaModel.MoveSize,
                OriginStateCode = verifyServiceAreaModel.OriginState,
                DestinationStateCode = verifyServiceAreaModel.DestinationState
            };

            // Query LMP API
            QlabExecutor executor = new QlabExecutor(new QlabConsumer(), _telemetry);
            QuoteResult instaQuoteResult = executor.GetMoveQuote(quoteRequest);
            return instaQuoteResult;
        }

        /// <summary>
        /// Does a quick check on the zip code is a valid zip by Bing/Google
        /// </summary>
        /// <param name="zipCode">5 digit Zip Code</param>
        /// <returns>Boolean if the zip code is valid</returns>
        [HttpGet]
        [Route("Home/ZipValidator")]
        public IActionResult ZipValidator(string zipCode)
        {
            if (zipCode.Length == 4)
            {
                zipCode = "0" + zipCode;
            }

            try
            {
                var zipResult = new MappingWrapper(_telemetry).GetCityStateForZipCodeAsync(zipCode).Result;
                if (!string.IsNullOrWhiteSpace(zipResult.City) && !string.IsNullOrWhiteSpace(zipResult.State))
                {
                    return Content("True");
                }
            }
            catch (Exception ex)
            {
                _telemetry.TrackException(ex);
            }

            return Content("False");
        }

        /// <summary>
        /// Does a quick check on the distance between zip codes to determine if the move is actually local
        /// </summary>
        /// <param name="zipRequest">Information about user entered Zip Codes</param>
        /// <returns>Miles between Zip Codes (decimal)</returns>
        [HttpPost]
        [Route("Home/ZipCheck")]
        public IActionResult ZipCheck(ZipRequest zipRequest)
        {
            if (zipRequest?.OriginZip == null || zipRequest.DestZip == null || string.IsNullOrEmpty(zipRequest.OriginZip) || string.IsNullOrEmpty(zipRequest.DestZip))
            {
                return BadRequest();
            }

            LocationLookupResult lookupResult = new LocationLookupResult();

            // Check for zipcode that starts with a zero but got clipped
            string originZip = zipRequest.OriginZip;
            if (zipRequest.OriginZip.Length == 4)
            {
                originZip = "0" + zipRequest.OriginZip;
            }

            string destinationZip = zipRequest.DestZip;
            if (zipRequest.DestZip.Length == 4)
            {
                destinationZip = "0" + zipRequest.DestZip;
            }

            try
            {
                // Get State of Origin Zip
                var originZipResult = new MappingWrapper(_telemetry).GetCityStateForZipCodeAsync(originZip).Result;
                lookupResult.OriginCity = originZipResult.City;
                lookupResult.OriginState = originZipResult.State;

                // Get State of Destination Zip
                var destinationZipResult = new MappingWrapper(_telemetry).GetCityStateForZipCodeAsync(destinationZip).Result;
                lookupResult.DestinationCity = destinationZipResult.City;
                lookupResult.DestinationState = destinationZipResult.State;

                // Get travel distance between zips
                double travelDistance = new MappingWrapper(_telemetry).GetDistanceBetweenTwoZipCodes(originZip, destinationZip).Result;
                lookupResult.Distance = travelDistance;

                // Dictionary to store distance stat
                Dictionary<string, string> distanceDictionary = new Dictionary<string, string>
                {
                    {"TravelDistance", travelDistance.ToString()}
                };
                // Log event that this will be a local when the user completes entering data
                if (travelDistance < 50)
                {
                    _telemetry.TrackEvent("Local Move based on distance", distanceDictionary);
                }
                else
                {
                    _telemetry.TrackEvent("Interstate Move based on distance", distanceDictionary);
                }
                // Store metric of distance between zip codes
                _telemetry.TrackMetric("Move Distance", travelDistance);

                return Json(lookupResult);

            }
            catch (Exception ex)
            {
                _telemetry.TrackException(ex);
            }
            // One of the zip codes didn't exist
            return Content("-1");
        }

        [HttpPost]
        [Route("Home/CollectContactInfo")]
        public async Task<IActionResult> CollectContactInfo(ContactInfoVM userData)
        {
            // Flatten the data for storing into the database
            FunnelContactDetail contactDetail = new FunnelContactDetail
            {
                CreatedAt = DateTime.Now,
                Steps = userData.Steps,
                ContactFormName = userData.ContactFormName,
                MoveDate = userData.MoveDate,
                Name = userData.Name,
                Phone = userData.Phone,
                Email = userData.Email.Replace(" ", string.Empty),
                OfficeCompanyName = userData.OfficeCompanyName,
                OfficeTimeframe = userData.OfficeTimeframe,
                OfficeTellAboutProject = userData.OfficeTellAboutProject,
                OriginAddress = userData.OriginAddress,
                DestinationAddress = userData.DestinationAddress,
                PromoCode = userData.PromoCode,
                Source = userData.Source
            };
            // Attempt to parse zips if they exist
            int.TryParse(userData.OriginZip, out var originZipInt);
            contactDetail.OriginZip = originZipInt;
            int.TryParse(userData.DestZip, out var destinationZipInt);
            contactDetail.DestinationZip = destinationZipInt;
            // Add in Salesperson info
            if (contactDetail.OriginZip.HasValue)
            {
                if (zipRepo.IsInServiceArea(contactDetail.OriginZip.Value.ToString()).Result)
                {
                    SalesPerson estimator = zipRepo.SalesPersonForZip(contactDetail.OriginZip.Value.ToString()).Result;
                    contactDetail.SalesPersonName = estimator.Name;
                    contactDetail.SalesPersonLocationCode = estimator.LocationCode;
                }
            }

            string[] stepsArray = userData.Steps.Split(',');
            contactDetail.Type = stepsArray[0] == "Other" ? stepsArray[1] : stepsArray[0];

            // Check if Move Date is null, set to Jan 1, 1970
            if (contactDetail.MoveDate < new DateTime(1970, 1, 1))
            {
                contactDetail.MoveDate = new DateTime(1970, 1, 1);
            }

            // Only store info in production
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME")))
            {
                // Store the contact info in the database
                SalesZipRepository repo = new SalesZipRepository(_connectionString, _telemetry);
                repo.StoreContactInfo(contactDetail);

                // Send notification to Teams channel
                SendRequestForContactNotificationToTeams(contactDetail);

                // Create ticket in Zoho to request contact
                await CreateZohoTicketForRfc(contactDetail);
            }

            return Content("");
        }

        [HttpPost]
        [Route("Home/CollectAppointmentData")]
        public async Task<IActionResult> CollectAppointmentData(ContactInfoVM userData)
        {
            // Only store info in production
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME")))
            {
                // Get Salesperson
                SalesZipRepository repo = new SalesZipRepository(_connectionString, _telemetry);
                SalesPerson salesPerson = await repo.SalesPersonByName(userData.EstimatorName);

                // Track appointment booking to salesperson
                Dictionary<string, string> apptBookedDictionary = new Dictionary<string, string> { { "Estimator", salesPerson.Name }, { "Appointment Datetime", userData.AppointmentDateTime.Value.ToString(CultureInfo.InvariantCulture) } };
                _telemetry.TrackEvent("Appointment Booked", apptBookedDictionary);

                // Send notification to Teams
                TeamsNotificationMessage notificationMessage =
                    new TeamsNotificationMessage
                    {
                        ApptTime = userData.AppointmentDateTime.Value.ToString("f") + " MST",
                        CustomerName = userData.Name,
                        CustomerEmail = userData.Email.Replace(" ", string.Empty),
                        CustomerPhone = userData.Phone,
                        Estimator = userData.EstimatorName,
                        OriginZip = int.TryParse(userData.OriginZip, out int originZipInt) ? originZipInt : 0,
                        DestinationZip = int.TryParse(userData.DestZip, out int destZipInt) ? destZipInt : 0
                    };

                // Add appointment info to FunnelContactDetails
                repo.AddAppointmentToContactDetail(userData);

                // Check if the appointment is set on the assumed calendar
                if (userData.ContactFormName == "assumedApptCalendar")
                {
                    SendAssumeddAppointmentNotificationToTeams(notificationMessage);
                    // Create appointment on the QLAB calendar
                    CreateAppointmentOnGoogleCalendar(userData, "baileysqlab@gmail.com");

                    // Create ticket in Zoho for Assumed Appointment
                    await CreateAssumedAppointmentInfoInZoho(userData);
                }
                else
                {
                    userData.ContactFormName = "ApptCalendar";

                    // Get calendar for salesperson
                    var calendarId = salesPerson.CalendarId;
                    // Create appointment on the calendar
                    CreateAppointmentOnGoogleCalendar(userData, calendarId);

                    // Send event to queue to be processed
                    ServiceBusExecutor serviceBusExecutor = new ServiceBusExecutor(_telemetry);
                    serviceBusExecutor.AddOpportunityToAdderQueue(userData);

                    // Send Confirmed Appointment to Teams
                    SendConfirmedAppointmentNotificationToTeams(notificationMessage);

                    // Send Confirmed Appointment to Zoho
                    await CreateZohoTicketForConfirmedBookingAsync(userData);
                }
            }

            return Content("");
        }

        [HttpPost]
        [Route("Home/CollectYemboRequest")]
        public async Task<IActionResult> CollectYemboRequest(ContactInfoVM userData)
        {
            // Only trigger in production
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME")))
            {
                // Send off SendGrid welcome email
                await SendWelcomeToYemboEmail(userData);

                // Create Zoho ticket for Yembo request
                await CreateZohoTicketForYemboRequest(userData);
            }

            // Hopefully all went well
            return Content("");
        }

        [HttpPost]
        [Route("Home/CollectHomieRequest")]
        public async Task<IActionResult> CollectHomieRequest(ContactInfoVM userData)
        {
            // Only trigger in production
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("WEBSITE_SITE_NAME")))
            {
                FullNameParser nameParser = new FullNameParser(userData.Name);
                nameParser.Parse();

                // Subscribe to Marketing list for Homie
                MarketingContactRoot marketingCampaignContact =
                    new MarketingContactRoot
                    {
                        ListIds =
                            new List<string>() { "316bbd38-af30-4264-8eb9-818551bc7813" },
                        Contacts = new List<MarketingContact>()
                        {
                            new()
                            {
                                Email = userData.Email,
                                FirstName = nameParser.FirstName,
                                LastName = nameParser.LastName,
                                CustomFields = new CustomFields()
                                {
                                    Name = userData.Name
                                }
                            }
                        }
                    };

                await _sendgridEmailService.AddMarketingCampaignContact(marketingCampaignContact);

                ContactCreationRequest contactCreationRequest = new ContactCreationRequest()
                {
                    Contact = new CreateContactRequest()
                    {
                        FirstName = nameParser.FirstName,
                        LastName = nameParser.LastName,
                        Email = userData.Email,
                        FieldValues = new List<FieldValueContactCreation>()
                        {
                            new()
                            {
                                Field = "3",
                                Value = Convert.ToBase64String(Encoding.UTF8.GetBytes(userData.Email))
                            }
                        }
                    }
                };
                await _activeCampaignService.TryCreateNewContactAsync(contactCreationRequest);
            }

            // Hopefully all went well
            return Content("");
        }

        private async Task CreateZohoTicketForConfirmedBookingAsync(ContactInfoVM userData)
        {
            HttpClient zohoApiClient = _clientFactory.CreateClient("ZohoDeskApi");
            SecretClient keyvaultSecretClient = new SecretClient(new Uri(Environment.GetEnvironmentVariable("BaileysInternalVaultUri")), new DefaultAzureCredential());
            ZohoService zohoApiService = new ZohoService(zohoApiClient, keyvaultSecretClient);

            // Get Zoho Token
            var zohoHttpClient = _clientFactory.CreateClient("ZohoDeskApi");
            ZohoTokenService zohoTokenService = new ZohoTokenService(zohoHttpClient);

            var accessToken = await zohoTokenService.GetToken();
            zohoApiService.SetOauthAccessToken(accessToken.AccessToken);

            // Check for existing contact
            FullNameParser nameParser = new FullNameParser(userData.Name);
            nameParser.Parse();
            string lastName = "Not Given";
            if (!string.IsNullOrEmpty(nameParser.LastName))
            {
                lastName = nameParser.LastName;
            }

            string zohoContactId = await zohoApiService.CheckForExistingContactAsync(nameParser.FirstName, lastName, userData.Phone);

            // Need an existing contact
            if (string.IsNullOrEmpty(zohoContactId))
            {
                // Create a contact for this request for contact
                ZohoContactCreateRequest contact = new ZohoContactCreateRequest
                {
                    FirstName = nameParser.FirstName,
                    LastName = lastName,
                    Email = userData.Email,
                    Phone = userData.Phone
                };

                if (!string.IsNullOrEmpty(userData.OriginZip))
                {
                    contact.Zip = userData.OriginZip;
                }

                zohoContactId = await zohoApiService.CreateNewContactAsync(contact);
            }

            // Lookup potential lead in Zoho that's associated with this contact
            var ticketList = await zohoApiService.GetExistingTicketsForContact(zohoContactId);
            List<ZohoTicketLookupResult> zohoTickets = new List<ZohoTicketLookupResult>();

            // Most likely no existing leads for this contact
            if (ticketList != null && ticketList.Any())
            {
                foreach (var ticketId in ticketList)
                {
                    zohoTickets.Add(await zohoApiService.GetTicketDetail(ticketId));
                }
            }

            if (zohoTickets.Any(x => x.Cf.CustomerType == "Lead" && x.StatusType == "Open"))
            {
                List<ZohoTicketLookupResult> openLeadTickets =
                    zohoTickets.Where(x => x.Cf.CustomerType == "Lead" && x.StatusType == "Open").ToList();
                // Close these tickets that are still open
                foreach (var leadTicket in openLeadTickets)
                {
                    await zohoApiService.CloseExistingTickets(new List<string>() { leadTicket.Id });
                }
            }

            // Create Zoho Ticket
            ZohoTicket zohoTicket = new ZohoTicket { DepartmentId = "419911000000006907" };

            ZohoTicketCustomFields customFields = new ZohoTicketCustomFields
            { CustomerType = "Lead", PriorityViewable = "true" };
            string[] stepsArray = userData.Steps.Split(',');
            customFields.Product = stepsArray[0] == "Other" ? stepsArray[1] : stepsArray[0];

            zohoTicket.ContactId = zohoContactId;
            zohoTicket.Cf = customFields;
            zohoTicket.Priority = "Medium";

            // Due in 4 hours or EOB
            DateTime dueDateTime = DateTime.Now.AddHours(4);
            if (dueDateTime.TimeOfDay.Hours > 17)
            {
                DateTime endOfBusinessDay = DateTime.Now.Date;
                dueDateTime = endOfBusinessDay.AddHours(17);
            }

            zohoTicket.DueDate = dueDateTime.ToUniversalTime();

            zohoTicket.Subject = $"Appointment Confirmed (Web Funnel) from {userData.Name}";

            StringBuilder sb = new StringBuilder();
            sb.Append("<div><br /></div>");
            sb.Append("<div><b>Contact Info:</b></div>");
            sb.Append($"<div><b>Request Type:</b> {customFields.CustomerType}</div>");
            sb.Append($"<div><b>Name:</b> {userData.Name}</div>");
            sb.Append($"<div><b>Email:</b> {userData.Email}</div>");
            sb.Append($"<div><b>Phone:</b> {userData.Phone}</div>");
            sb.Append("<div><br /></div>");
            sb.Append("<div><br /></div>");
            sb.Append("<div><b>Address Info:</b></div>");
            sb.Append($"<div><b>Origin Address:</b> {userData.OriginAddress}</div>");
            sb.Append($"<div><b>Origin City:</b> {userData.OriginCity}</div>");
            sb.Append($"<div><b>Origin State:</b> {userData.OriginState}</div>");
            sb.Append($"<div><b>Origin Zip:</b> {userData.OriginZip}</div>");
            sb.Append($"<div><b>Destination Address:</b> {userData.DestinationAddress}</div>");
            sb.Append($"<div><b>Destination City:</b> {userData.DestinationCity}</div>");
            sb.Append($"<div><b>Destination State:</b> {userData.DestinationState}</div>");
            sb.Append($"<div><b>Destination Zip:</b> {userData.DestZip}</div>");
            sb.Append($"<div><b>Move Date:</b> {userData.MoveDate}</div>");
            if (userData.AppointmentDateTime.HasValue)
            {
                sb.Append($"<div><b>Requested Appointment Date/Time:</b> {userData.AppointmentDateTime.Value}</div>");
            }
            else
            {
                sb.Append("<div>Error: The requested appointment date/time was missing</div>");

            }

            sb.Append("<div><br /></div>");
            sb.Append("<div><br /></div>");
            sb.Append("Description:");
            sb.Append("<div><br /></div>");
            sb.Append("<div>This lead was created through baileysallied.com. The customer selected their own appointment time, and an entry has been made on the salesman's calendar.</div>");
            sb.Append("<div><br /></div>");
            sb.Append("<div>QLAB records have also been created and will appear in Zoho shortly, but please create a QLAB appointment for the requested date and time on the opportunity once it appears. Please allow 10-15 minutes to appear in Zoho..</div>");

            zohoTicket.Description = sb.ToString();

            await zohoApiService.AddTicket(zohoTicket);
            _telemetry.TrackEvent("SentConfirmedBookingToZoho");
        }

        private async Task CreateZohoTicketForRfc(FunnelContactDetail contactDetail)
        {
            HttpClient zohoApiClient = _clientFactory.CreateClient("ZohoDeskApi");
            SecretClient keyvaultSecretClient = new SecretClient(new Uri(Environment.GetEnvironmentVariable("BaileysInternalVaultUri")), new DefaultAzureCredential());
            ZohoService zohoApiService = new ZohoService(zohoApiClient, keyvaultSecretClient);

            // Get Zoho Token
            var zohoHttpClient = _clientFactory.CreateClient("ZohoDeskApi");
            ZohoTokenService zohoTokenService = new ZohoTokenService(zohoHttpClient);

            var accessToken = await zohoTokenService.GetToken();
            zohoApiService.SetOauthAccessToken(accessToken.AccessToken);

            FullNameParser nameParser = new FullNameParser(contactDetail.Name);
            nameParser.Parse();
            string lastName = "Not Given";
            if (!string.IsNullOrEmpty(nameParser.LastName))
            {
                lastName = nameParser.LastName;
            }

            string zohoContactId = await zohoApiService.CheckForExistingContactAsync(nameParser.FirstName, lastName, contactDetail.Phone);

            if (string.IsNullOrEmpty(zohoContactId))
            {
                // Create a contact for this request for contact
                ZohoContactCreateRequest contact = new ZohoContactCreateRequest
                {
                    FirstName = nameParser.FirstName,
                    LastName = lastName,
                    Email = contactDetail.Email,
                    Phone = contactDetail.Phone
                };

                if (contactDetail.OriginZip.HasValue)
                {
                    contact.Zip = contactDetail.OriginZip.Value.ToString();
                }

                zohoContactId = await zohoApiService.CreateNewContactAsync(contact);
            }

            // Create Zoho Ticket
            ZohoTicket zohoTicket = new ZohoTicket { DepartmentId = "419911000000006907" };

            ZohoTicketCustomFields customFields = new ZohoTicketCustomFields { CustomerType = "Lead", PriorityViewable = "true" };
            string[] stepsArray = contactDetail.Steps.Split(',');
            if (stepsArray[0] == "Other")
            {
                customFields.Product = stepsArray[1];
            }
            else
            {
                if (stepsArray[0] == "House" || stepsArray[0] == "Apartment")
                {
                    // Check for International
                    if (stepsArray.Length >= 3 && stepsArray[2] == "International")
                    {
                        customFields.Product = stepsArray[2];
                    }
                    else
                    {
                        // Standard move
                        customFields.Product = stepsArray[0];
                    }
                }
                else
                {
                    customFields.Product = stepsArray[0]; // Office
                }
            }

            zohoTicket.ContactId = zohoContactId;
            zohoTicket.Cf = customFields;
            zohoTicket.Priority = "Medium";

            // Due in 4 hours or EOB
            DateTime dueDateTime = DateTime.Now.AddHours(4);
            if (dueDateTime.TimeOfDay.Hours > 17)
            {
                DateTime endOfBusinessDay = DateTime.Now.Date;
                dueDateTime = endOfBusinessDay.AddHours(17);
            }
            zohoTicket.DueDate = dueDateTime.ToUniversalTime();

            // Check if this is an International contact
            if (customFields.Product == "International")
            {
                zohoTicket.Subject = $"International {customFields.CustomerType} Inquiry from {contactDetail.Name}";
            }
            else
            {
                zohoTicket.Subject = $"{customFields.CustomerType} Inquiry from {contactDetail.Name}";
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("<div><br /></div>");
            sb.Append("<div><b>Contact Info:</b></div>");
            sb.Append($"<div><b>Request Type:</b> {customFields.Product}</div>");
            sb.Append($"<div><b>Name:</b> {contactDetail.Name}</div>");
            sb.Append($"<div><b>Email:</b> {contactDetail.Email}</div>");
            sb.Append($"<div><b>Phone:</b> {contactDetail.Phone}</div>");
            if (customFields.Product == "Home" || customFields.Product == "Apartment")
            {
                sb.Append("<div><br /></div>");
                sb.Append("<div><br /></div>");
                sb.Append("<div><b>Address Info:</b></div>");
                sb.Append($"<div><b>Origin Address:</b> {contactDetail.OriginAddress}</div>");
                sb.Append($"<div><b>Origin City:</b> {contactDetail.OriginCity}</div>");
                sb.Append($"<div><b>Origin State:</b> {contactDetail.OriginState}</div>");
                sb.Append($"<div><b>Origin Zip:</b> {contactDetail.OriginZip}</div>");
                sb.Append($"<div><b>Destination Address:</b> {contactDetail.DestinationAddress}</div>");
                sb.Append($"<div><b>Destination City:</b> {contactDetail.DestinationCity}</div>");
                sb.Append($"<div><b>Destination State:</b> {contactDetail.DestinationState}</div>");
                sb.Append($"<div><b>Destination Zip:</b> {contactDetail.DestinationZip}</div>");
                sb.Append($"<div><b>Move Date:</b> {contactDetail.MoveDate}</div>");
            }
            if (customFields.Product == "Office/Industrial")
            {
                sb.Append("<div><br /></div>");
                sb.Append("<div><br /></div>");
                sb.Append("<div><b>Company Specific Info:</b></div>");
                sb.Append($"<div><b>Company Name:</b> {contactDetail.OfficeCompanyName}</div>");
                sb.Append($"<div><b>Timeframe:</b> {contactDetail.OfficeTimeframe}</div>");
                sb.Append($"<div><b>Office Move Comments:</b> {contactDetail.OfficeTellAboutProject}</div>");
            }

            zohoTicket.Description = sb.ToString();

            var jsonOptions = new JsonSerializerOptions { IgnoreNullValues = true };
            jsonOptions.Converters.Add(new ZohoDatetimeConverter());

            string jsonRequest = System.Text.Json.JsonSerializer.Serialize(zohoTicket, jsonOptions);

            Dictionary<string, string> rfcJsonDictionary = new Dictionary<string, string>
            {
                {"jsonRequest", jsonRequest}
            };
            _telemetry.TrackTrace("ZohoRfcJson", rfcJsonDictionary);

            var httpResponseMessage = await zohoApiService.AddTicket(zohoTicket);


            Dictionary<string, string> httpResponseDictionary = new Dictionary<string, string>
            {
                {"httpResponseReason", httpResponseMessage.ReasonPhrase},
                {"httpContent", await httpResponseMessage.Content.ReadAsStringAsync()},
                {"httpRequestMessage", await httpResponseMessage.RequestMessage.Content.ReadAsStringAsync() }
            };

            try
            {
                var errorJson = await httpResponseMessage.Content.ReadFromJsonAsync<ZohoServerErrorMessage>();
                if (errorJson != null)
                {
                    httpResponseDictionary.Add("serverErrorCode", errorJson.ErrorCode);
                    httpResponseDictionary.Add("serverErrorMessage", errorJson.Message);
                }
            }
            catch (Exception ex)
            {

            }

            _telemetry.TrackTrace("ZohoHttpResponse", httpResponseDictionary);
            _telemetry.TrackEvent("SentRequestForContactToZoho");
        }

        private async Task CreateZohoTicketForYemboRequest(ContactInfoVM userData)
        {
            HttpClient zohoApiClient = _clientFactory.CreateClient("ZohoDeskApi");
            SecretClient keyvaultSecretClient = new SecretClient(new Uri(Environment.GetEnvironmentVariable("BaileysInternalVaultUri")), new DefaultAzureCredential());
            ZohoService zohoApiService = new ZohoService(zohoApiClient, keyvaultSecretClient);

            // Get Zoho Token
            var zohoHttpClient = _clientFactory.CreateClient("ZohoDeskApi");
            ZohoTokenService zohoTokenService = new ZohoTokenService(zohoHttpClient);

            var accessToken = await zohoTokenService.GetToken();
            zohoApiService.SetOauthAccessToken(accessToken.AccessToken);

            // Check for existing contact
            FullNameParser nameParser = new FullNameParser(userData.Name);
            nameParser.Parse();
            string lastName = "Not Given";
            if (!string.IsNullOrEmpty(nameParser.LastName))
            {
                lastName = nameParser.LastName;
            }

            string zohoContactId = await zohoApiService.CheckForExistingContactAsync(nameParser.FirstName, lastName, userData.Phone);

            // Need an existing contact
            if (string.IsNullOrEmpty(zohoContactId))
            {
                // Create a contact for this request for contact
                ZohoContactCreateRequest contact = new ZohoContactCreateRequest
                {
                    FirstName = nameParser.FirstName,
                    LastName = lastName,
                    Email = userData.Email,
                    Phone = userData.Phone
                };

                if (!string.IsNullOrEmpty(userData.OriginZip))
                {
                    contact.Zip = userData.OriginZip;
                }

                zohoContactId = await zohoApiService.CreateNewContactAsync(contact);
            }

            // Lookup potential lead in Zoho that's associated with this contact
            var ticketList = await zohoApiService.GetExistingTicketsForContact(zohoContactId);
            List<ZohoTicketLookupResult> zohoTickets = new List<ZohoTicketLookupResult>();

            // Most likely no existing leads for this contact
            if (ticketList != null && ticketList.Any())
            {
                foreach (var ticketId in ticketList)
                {
                    zohoTickets.Add(await zohoApiService.GetTicketDetail(ticketId));
                }
            }

            if (zohoTickets.Any(x => x.Cf.CustomerType == "Lead" && x.StatusType == "Open"))
            {
                List<ZohoTicketLookupResult> openLeadTickets =
                    zohoTickets.Where(x => x.Cf.CustomerType == "Lead" && x.StatusType == "Open").ToList();
                // Close these tickets that are still open
                foreach (var leadTicket in openLeadTickets)
                {
                    await zohoApiService.CloseExistingTickets(new List<string>() { leadTicket.Id });
                }
            }

            // Create Zoho Ticket
            ZohoTicket zohoTicket = new ZohoTicket { DepartmentId = "419911000000006907" };

            ZohoTicketCustomFields customFields = new ZohoTicketCustomFields
            { CustomerType = "Yembo Request", PriorityViewable = "true" };
            string[] stepsArray = userData.Steps.Split(',');
            customFields.Product = stepsArray[0] == "Other" ? stepsArray[1] : stepsArray[0];

            zohoTicket.ContactId = zohoContactId;
            zohoTicket.Cf = customFields;
            zohoTicket.Priority = "Medium";

            // Due in 4 hours or EOB
            DateTime dueDateTime = DateTime.Now.AddHours(4);
            if (dueDateTime.TimeOfDay.Hours > 17)
            {
                DateTime endOfBusinessDay = DateTime.Now.Date;
                dueDateTime = endOfBusinessDay.AddHours(17);
            }

            zohoTicket.DueDate = dueDateTime.ToUniversalTime();

            zohoTicket.Subject = $"Yembo Requested (Web Funnel) from {userData.Name}";

            StringBuilder sb = new StringBuilder();
            sb.Append("<div><br /></div>");
            sb.Append("<div><b>Contact Info:</b></div>");
            sb.Append($"<div><b>Request Type:</b> {customFields.CustomerType}</div>");
            sb.Append($"<div><b>Name:</b> {userData.Name}</div>");
            sb.Append($"<div><b>Email:</b> {userData.Email}</div>");
            sb.Append($"<div><b>Phone:</b> {userData.Phone}</div>");
            sb.Append("<div><br /></div>");
            sb.Append("<div><br /></div>");
            sb.Append("<div><b>Address Info:</b></div>");
            sb.Append($"<div><b>Origin Address:</b> {userData.OriginAddress}</div>");
            sb.Append($"<div><b>Origin City:</b> {userData.OriginCity}</div>");
            sb.Append($"<div><b>Origin State:</b> {userData.OriginState}</div>");
            sb.Append($"<div><b>Origin Zip:</b> {userData.OriginZip}</div>");
            sb.Append($"<div><b>Destination Address:</b> {userData.DestinationAddress}</div>");
            sb.Append($"<div><b>Destination City:</b> {userData.DestinationCity}</div>");
            sb.Append($"<div><b>Destination State:</b> {userData.DestinationState}</div>");
            sb.Append($"<div><b>Destination Zip:</b> {userData.DestZip}</div>");
            sb.Append($"<div><b>Move Date:</b> {userData.MoveDate}</div>");

            sb.Append("<div><br /></div>");
            sb.Append("<div><br /></div>");
            sb.Append("Description:");
            sb.Append("<div><br /></div>");
            sb.Append("<div>This is a placeholder ticket due to a Yembo Request being made via the Quote Funnel. This should be auto-deleted once a matching Opportunity get's created in Zoho.</div>");

            zohoTicket.Description = sb.ToString();

            await zohoApiService.AddTicket(zohoTicket);
            _telemetry.TrackEvent("SentYemboRequestTicketToZoho");
        }

        private async Task SendWelcomeToYemboEmail(ContactInfoVM infoVm)
        {
            try
            {
                // Try to parse out First Name
                FullNameParser nameParser = new FullNameParser(infoVm.Name);
                nameParser.Parse();
                string firstName = infoVm.Name;
                if (!string.IsNullOrEmpty(nameParser.FirstName))
                {
                    firstName = nameParser.FirstName;
                }

                YemboSendgridTokens tokens = new YemboSendgridTokens { FirstName = firstName };

                await _sendgridEmailService.SubscribeToTemplateAsync(infoVm.Email, "moveteam@baileysallied.com",
                    "Jake from Bailey's Moving", tokens, "d-d182798e248d4cbeb295b5c508931cd2");
            }
            catch (Exception ex)
            {
                _telemetry.TrackException(ex);
            }
        }

        private async Task CreateAssumedAppointmentInfoInZoho(ContactInfoVM userData)
        {
            HttpClient zohoApiClient = _clientFactory.CreateClient("ZohoDeskApi");
            SecretClient keyvaultSecretClient = new SecretClient(new Uri(Environment.GetEnvironmentVariable("BaileysInternalVaultUri")), new DefaultAzureCredential());
            ZohoService zohoApiService = new ZohoService(zohoApiClient, keyvaultSecretClient);

            // Get Zoho Token
            var zohoHttpClient = _clientFactory.CreateClient("ZohoDeskApi");
            ZohoTokenService zohoTokenService = new ZohoTokenService(zohoHttpClient);

            var accessToken = await zohoTokenService.GetToken();
            zohoApiService.SetOauthAccessToken(accessToken.AccessToken);

            // Check for existing contact due to previous lead creation
            FullNameParser nameParser = new FullNameParser(userData.Name);
            nameParser.Parse();
            string lastName = "Not Given";
            if (!string.IsNullOrEmpty(nameParser.LastName))
            {
                lastName = nameParser.LastName;
            }

            string zohoContactId = await zohoApiService.CheckForExistingContactAsync(nameParser.FirstName, lastName, userData.Phone);

            // Need an existing contact
            if (string.IsNullOrEmpty(zohoContactId))
            {
                // Create a contact for this request for contact
                ZohoContactCreateRequest contact = new ZohoContactCreateRequest
                {
                    FirstName = nameParser.FirstName,
                    LastName = lastName,
                    Email = userData.Email,
                    Phone = userData.Phone
                };

                if (!string.IsNullOrEmpty(userData.OriginZip))
                {
                    contact.Zip = userData.OriginZip;
                }

                zohoContactId = await zohoApiService.CreateNewContactAsync(contact);
            }

            // Lookup existing lead in Zoho that's associated with this contact
            var ticketList = await zohoApiService.GetExistingTicketsForContact(zohoContactId);
            List<ZohoTicketLookupResult> zohoTickets = new List<ZohoTicketLookupResult>();
            if (ticketList.Any())
            {
                foreach (var ticketId in ticketList)
                {
                    zohoTickets.Add(await zohoApiService.GetTicketDetail(ticketId));
                }
            }

            if (zohoTickets.Any(x => x.Cf.CustomerType == "Lead" && x.StatusType == "Open"))
            {
                List<ZohoTicketLookupResult> openLeadTickets =
                    zohoTickets.Where(x => x.Cf.CustomerType == "Lead" && x.StatusType == "Open").ToList();
                // Close these tickets that are still open
                foreach (var leadTicket in openLeadTickets)
                {
                    await zohoApiService.CloseExistingTickets(new List<string>() { leadTicket.Id });
                }
            }


            // Create Zoho Ticket
            ZohoTicket zohoTicket = new ZohoTicket { DepartmentId = "419911000000006907" };

            ZohoTicketCustomFields customFields = new ZohoTicketCustomFields
            { CustomerType = "Lead", PriorityViewable = "true" };
            string[] stepsArray = userData.Steps.Split(',');
            customFields.Product = stepsArray[0] == "Other" ? stepsArray[1] : stepsArray[0];

            zohoTicket.ContactId = zohoContactId;
            zohoTicket.Cf = customFields;
            zohoTicket.Priority = "Medium";

            // Due in 4 hours or EOB
            DateTime dueDateTime = DateTime.Now.AddHours(4);
            if (dueDateTime.TimeOfDay.Hours > 17)
            {
                DateTime endOfBusinessDay = DateTime.Now.Date;
                dueDateTime = endOfBusinessDay.AddHours(17);
            }

            zohoTicket.DueDate = dueDateTime.ToUniversalTime();

            zohoTicket.Subject = $"{customFields.CustomerType} Inquiry from {userData.Name}";

            StringBuilder sb = new StringBuilder();
            sb.Append("<div><br /></div>");
            sb.Append("<div><b>Contact Info:</b></div>");
            sb.Append($"<div><b>Request Type:</b> {customFields.CustomerType}</div>");
            sb.Append($"<div><b>Name:</b> {userData.Name}</div>");
            sb.Append($"<div><b>Email:</b> {userData.Email}</div>");
            sb.Append($"<div><b>Phone:</b> {userData.Phone}</div>");
            sb.Append("<div><br /></div>");
            sb.Append("<div><br /></div>");
            sb.Append("<div><b>Address Info:</b></div>");
            sb.Append($"<div><b>Origin Address:</b> {userData.OriginAddress}</div>");
            sb.Append($"<div><b>Origin City:</b> {userData.OriginCity}</div>");
            sb.Append($"<div><b>Origin State:</b> {userData.OriginState}</div>");
            sb.Append($"<div><b>Origin Zip:</b> {userData.OriginZip}</div>");
            sb.Append($"<div><b>Destination Address:</b> {userData.DestinationAddress}</div>");
            sb.Append($"<div><b>Destination City:</b> {userData.DestinationCity}</div>");
            sb.Append($"<div><b>Destination State:</b> {userData.DestinationState}</div>");
            sb.Append($"<div><b>Destination Zip:</b> {userData.DestZip}</div>");
            sb.Append($"<div><b>Move Date:</b> {userData.MoveDate}</div>");

            zohoTicket.Description = sb.ToString();

            await zohoApiService.AddTicket(zohoTicket);
            _telemetry.TrackEvent("SentAssumedAppointmentToZoho");
        }

        private string CreateAppointmentOnGoogleCalendar(ContactInfoVM userData, string calendarId)
        {
            // Load in json creds
            string jsonCredString = System.IO.File.ReadAllText(System.AppContext.BaseDirectory + "\\gcal-credential.json");
            var credentialParameters =
                NewtonsoftJsonSerializer.Instance.Deserialize<JsonCredentialParameters>(jsonCredString);
            var initializer = new ServiceAccountCredential.Initializer(credentialParameters.ClientEmail)
            {
                Scopes = new[] { CalendarService.Scope.Calendar, CalendarService.Scope.CalendarReadonly },
                User = "calendar-reader@calendar-dump.iam.gserviceaccount.com"
            };
            var cred = new ServiceAccountCredential(initializer.FromPrivateKey(credentialParameters.PrivateKey));

            CalendarService calendarService = new CalendarService(
                new Google.Apis.Services.BaseClientService.Initializer
                {
                    HttpClientInitializer = cred,
                    ApplicationName = "Web Funnel"
                });

            // Create appointment
            Event newEvent = new Event();
            newEvent.Start =
                new EventDateTime { DateTime = userData.AppointmentDateTime, TimeZone = "America/Denver" };
            newEvent.End = new EventDateTime
            {
                DateTime = userData.AppointmentDateTime.Value.AddHours(1),
                TimeZone = "America/Denver"
            };

            newEvent.Summary = $"{userData.Name} / {userData.MoveDistanceType} / IN-HOME";
            newEvent.Description =
                $"Customer Name: {userData.Name}\nEmail: {userData.Email.Replace(" ", string.Empty)}\nPhone: {userData.Phone}\nDestination Addr: {userData.DestinationAddress} {userData.DestinationCity}, {userData.DestinationState} {userData.DestZip}\nRequested Move Date: {userData.MoveDateFormatted}\nMove Type: {userData.MoveType}\nMove Size: {userData.MoveSize}\nAppointment Origin: Generated by WebFunnel\nPlease Enter In QLAB";
            newEvent.Location =
                $"{userData.OriginAddress} {userData.OriginCity}, {userData.OriginState} {userData.OriginZip}";

            // Execute request
            var appointmentGoogleRequest = calendarService.Events.Insert(newEvent, calendarId);
            var result = appointmentGoogleRequest.ExecuteAsync();

            // Return id of the event
            return result.Result.Id;
        }

        private HttpStatusCode SendRequestForContactNotificationToTeams(FunnelContactDetail contactDetail)
        {
            var client = new RestClient("https://prod-41.westus.logic.azure.com");
            var request = new RestRequest("workflows/eebf18ebc92448a08d2b5bf061db857a/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=QMy3k3FUDXpWtEkv1hwScdGLy4qQnzPak6MOfzulM-U", Method.POST);

            request.AddJsonBody(contactDetail);
            var response = client.ExecuteAsync(request);
            return response.Result.StatusCode;
        }

        private HttpStatusCode SendConfirmedAppointmentNotificationToTeams(TeamsNotificationMessage message)
        {
            var client = new RestClient("https://prod-24.westus.logic.azure.com");
            var request = new RestRequest("workflows/fdbd9c9f1a45415b8949522c0a8e936d/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=96iHuPWtqfypi3ddy5UxOj_uPyBB2iOEzFmaxDR967o", Method.POST);

            request.AddJsonBody(message);
            request.AddHeader("Content-Type", "application/json");
            var response = client.ExecuteAsync(request);
            return response.Result.StatusCode;
        }

        private HttpStatusCode SendAssumeddAppointmentNotificationToTeams(TeamsNotificationMessage message)
        {
            var client = new RestClient("https://prod-46.westus.logic.azure.com");
            var request = new RestRequest("workflows/dc0ab4d4b6f6470d99a21123abc8902a/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=1qFrpZczUNCPlNrdaszEmRhQEFMYKPrwnFCm7WHE4-I", Method.POST);

            request.AddJsonBody(message);
            request.AddHeader("Content-Type", "application/json");
            var response = client.ExecuteAsync(request);
            return response.Result.StatusCode;
        }

        [HttpPost]
        [Route("Home/GetAvailableDatesAsync")]
        public async Task<IActionResult> GetAvailableDatesAsync(VerifyServiceResult userData)
        {
            SalesZipRepository repo = new SalesZipRepository(_connectionString, _telemetry);
            SalesPerson salesPerson = await repo.SalesPersonByName(userData.EstimatorName);
            List<SalesScheduleAvailibility> availibilityList = await repo.GetAvailbilityForSalesPerson(salesPerson.Id.ToString());

            List<DateTime> appointmentStartTimes = new List<DateTime>();
            if (userData.AppointmentLookupDate == null)
            {
                // Get all appointment start times for the next 30 days
                appointmentStartTimes = GetAvaiableForSalesPerson(salesPerson, availibilityList, DateTime.Today,
                    DateTime.Today.AddDays(31));
                ViewData["estimator"] = salesPerson.Name;
                return PartialView("AppointmentSummary", appointmentStartTimes.Take(5));
            }
            // Get appointments for a single day
            appointmentStartTimes = GetAvaiableForSalesPerson(salesPerson, availibilityList, userData.AppointmentLookupDate.Value.Date,
                userData.AppointmentLookupDate.Value.Date.AddDays(1));
            appointmentStartTimes = appointmentStartTimes.Where(x => x.Date == userData.AppointmentLookupDate.Value.Date)
                .ToList();
            ViewData["estimator"] = salesPerson.Name;
            return PartialView("AppointmentSummary", appointmentStartTimes);
        }

        [HttpPost]
        [Route("Home/GetAvailableDatesJsonAsync")]
        public async Task<IActionResult> GetAvailableDatesJsonAsync(VerifyServiceResult userData)
        {
            SalesZipRepository repo = new SalesZipRepository(_connectionString, _telemetry);
            SalesPerson salesPerson = await repo.SalesPersonByName(userData.EstimatorName);
            List<SalesScheduleAvailibility> availibilityList = await repo.GetAvailbilityForSalesPerson(salesPerson.Id.ToString());

            List<DateTime> appointmentStartTimes = new List<DateTime>();
            if (userData.AppointmentLookupDate == null)
            {
                // Get all appointment start times for the next 30 days
                appointmentStartTimes = GetAvaiableForSalesPerson(salesPerson, availibilityList, DateTime.Today,
                    DateTime.Today.AddDays(31));
                ViewData["estimator"] = salesPerson.Name;
                return Json(appointmentStartTimes.Take(3));
            }
            // Get appointments for a single day
            appointmentStartTimes = GetAvaiableForSalesPerson(salesPerson, availibilityList, userData.AppointmentLookupDate.Value,
                userData.AppointmentLookupDate.Value.AddDays(1));
            appointmentStartTimes = appointmentStartTimes.Where(x => x.Date == userData.AppointmentLookupDate.Value)
                .ToList();
            ViewData["estimator"] = salesPerson.Name;
            return Json(appointmentStartTimes);
        }

        [HttpPost]
        [Route("Home/GetAvailabilityForSalesPersonJsonAsync")]
        public async Task<IActionResult> GetAvailabilityForSalesPersonJsonAsync(string estimatorName)
        {
            SalesZipRepository repo = new SalesZipRepository(_connectionString, _telemetry);
            SalesPerson salesPerson = await repo.SalesPersonByName(estimatorName);
            List<SalesScheduleAvailibility> availabilityList = await repo.GetAvailbilityForSalesPerson(salesPerson.Id.ToString());

            return Json(availabilityList);
        }


        /// <summary>
        /// Gets a list of currently available appointments in DateTime's for a Sales Person
        /// </summary>
        /// <param name="salesPerson">SalesPerson</param>
        /// <param name="availibilityList">List of the week's availibility to match LMP</param>
        /// <returns>List of DateTime which are valid appointment start times</returns>
        private List<DateTime> GetAvaiableForSalesPerson(SalesPerson salesPerson,
            List<SalesScheduleAvailibility> availibilityList, DateTime eventStartDate, DateTime eventEndDate)
        {
            // Load in json creds
            string currentDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string jsonCredString = System.IO.File.ReadAllText(currentDirectory + "\\gcal-credential.json");
            var credentialParameters =
                NewtonsoftJsonSerializer.Instance.Deserialize<JsonCredentialParameters>(jsonCredString);
            var initializer = new ServiceAccountCredential.Initializer(credentialParameters.ClientEmail)
            {
                Scopes = new[] { CalendarService.Scope.Calendar, CalendarService.Scope.CalendarReadonly }
            };
            var cred = new ServiceAccountCredential(initializer.FromPrivateKey(credentialParameters.PrivateKey));

            CalendarService calendarService = new CalendarService(
                new Google.Apis.Services.BaseClientService.Initializer
                {
                    HttpClientInitializer = cred,
                    ApplicationName = "Web Funnel"
                });

            // Check for paging and get the rest of the events if there are any
            List<Event> eventItems = new List<Event>(); // Keep the events in a list for paging purposes
            string pagingToken = string.Empty;
            do
            {
                // Make request to GCal
                var eventsListRequest = calendarService.Events.List(salesPerson.CalendarId);
                eventsListRequest.TimeMin = DateTime.Today;
                eventsListRequest.SingleEvents = true;
                eventsListRequest.MaxResults = 1500;
                eventsListRequest.TimeZone = "America/Denver";
                if (!string.IsNullOrEmpty(pagingToken))
                {
                    eventsListRequest.PageToken = pagingToken;
                }

                var events = eventsListRequest.Execute();
                eventItems.AddRange(events.Items); // Append the new events
                                                   // Check for paging token
                pagingToken = events.NextPageToken;
            } while (!string.IsNullOrEmpty(pagingToken));

            // Store a list of datetimes for valid appointment start times
            List<DateTime> validAppointmentTimes = new List<DateTime>();

            // Only get events that are for today for the next week
            List<Event> eventsInTimespan = eventItems.Where(x =>
                x.Start.DateTime != null && (x.Start.DateTime.Value >= eventStartDate.Date && x.Start.DateTime.Value < eventEndDate.Date)).ToList();

            eventItems = null; // NULL the list to allow it to be garbage collected

            // Default to selected start date
            DateTime currentCheckedDate = eventStartDate;
            int daysBetween = DaysBetween(eventStartDate, eventEndDate);
            // If this is a standard lookup, make use of the day's out
            if (daysBetween == 31)
            {
                currentCheckedDate = DateTime.Today.AddDays(salesPerson.ScheduleDaysOut);
            }
            for (int i = 0; i < daysBetween; i++)
            {
                // Increment through each day to check
                if (i != 0)
                {
                    currentCheckedDate = currentCheckedDate.AddDays(1);
                }

                // Skip days if not in the availibility schedule
                SalesScheduleAvailibility currentDateScheduleAvailibility = availibilityList.FirstOrDefault(x => x.DayOfWeek == (int)currentCheckedDate.DayOfWeek);
                if (currentDateScheduleAvailibility != null)
                {
                    // Get number of 30 min increments in this day
                    int increments = ((currentDateScheduleAvailibility.EndHour24 - currentDateScheduleAvailibility.StartHour24) * 2);
                    // Get the start for the appointment availibility search
                    TimeSpan currentStartTime = new TimeSpan(currentDateScheduleAvailibility.StartHour24, 0, 0);
                    for (int j = 0; j <= increments; j++)
                    {
                        // DEBUG
                        DateTime currentStartDateTime = currentCheckedDate.Add(currentStartTime);
                        if (j != 0)
                        {
                            // Increment by 30 minutes
                            currentStartTime = currentStartTime.Add(new TimeSpan(0, 30, 0));
                        }
                        // Determine if there is a open spot on this date and at this time
                        bool areEventsInProgressAtCurrentTime = eventsInTimespan.Any(x => x.Start.DateTime.Value.TimeOfDay <= currentStartTime && currentStartTime <= x.End.DateTime.Value.TimeOfDay && x.Start.DateTime.Value.Date == currentCheckedDate.Date);
                        // If there are not, see if an appointment would fit
                        if (!areEventsInProgressAtCurrentTime)
                        {
                            // Check to see if current start time is potentially the first
                            if (currentStartTime == new TimeSpan(currentDateScheduleAvailibility.StartHour24, 0, 0))
                            { // Can this fit
                                var beginningTimespan = currentStartTime.Add(new TimeSpan(1, 0, 0));
                                var endTimespan = currentStartTime.Add(new TimeSpan(0,
                                    salesPerson.ScheduleHoursApart + 1, salesPerson.ScheduleMinutesApart, 0));
                                var eventsInProgressSpecial = eventsInTimespan.Where(x =>
                                     x.Start.DateTime.Value.TimeOfDay <= beginningTimespan && x.Start.DateTime.Value.TimeOfDay < endTimespan && x.Start.DateTime.Value.Date == currentCheckedDate.Date);
                                if (!eventsInProgressSpecial.Any())
                                {
                                    DateTime workingDateTime = currentCheckedDate.Add(currentStartTime);

                                    validAppointmentTimes.Add(workingDateTime);
                                }
                            }
                            else if (currentStartTime == new TimeSpan(currentDateScheduleAvailibility.EndHour24, 0, 0)) // or the last appointment slot
                            {
                                var beginningTimespan = currentStartTime.Subtract(new TimeSpan(0,
                                    salesPerson.ScheduleHoursApart, salesPerson.ScheduleMinutesApart, 0));
                                var endTimespan = currentStartTime;
                                var eventsInProgressSpecial = eventsInTimespan.Where(x =>
                                   x.End.DateTime.Value.TimeOfDay >= beginningTimespan && x.End.DateTime.Value.TimeOfDay < endTimespan && x.Start.DateTime.Value.Date == currentCheckedDate.Date);
                                if (!eventsInProgressSpecial.Any())
                                {
                                    DateTime workingDateTime = currentCheckedDate.Add(currentStartTime);
                                    validAppointmentTimes.Add(workingDateTime);
                                }
                            }
                            else
                            {
                                // Normal appointment sizing determination
                                TimeSpan potentialAppointmentStartTime =
                                    currentStartTime.Subtract(new TimeSpan(salesPerson.ScheduleHoursApart,
                                        salesPerson.ScheduleMinutesApart, 0));
                                TimeSpan potentialAppointmentEndtime = currentStartTime.Add(new TimeSpan(1, 0, 0)).Add(new TimeSpan(salesPerson.ScheduleHoursApart, salesPerson.ScheduleMinutesApart, 0));

                                //DEBUG
                                DateTime temp1 = currentCheckedDate.Add(potentialAppointmentStartTime);
                                DateTime temp2 = currentCheckedDate.Add(potentialAppointmentEndtime);


                                // Check to make sure the upper range of hours apart doesn't go over the end of the day, needed for late afternoon appointments
                                if (potentialAppointmentEndtime >
                                    new TimeSpan(currentDateScheduleAvailibility.EndHour24, 0, 0))
                                {
                                    potentialAppointmentEndtime =
                                        new TimeSpan(currentDateScheduleAvailibility.EndHour24, 0, 0);
                                }
                                bool potentialAppointmentFits = !eventsInTimespan.Any(x => x.Start.DateTime.Value.TimeOfDay < potentialAppointmentEndtime && currentCheckedDate.Add(potentialAppointmentStartTime) < x.End.DateTime.Value && x.Start.DateTime.Value.Date == currentCheckedDate.Date);
                                if (potentialAppointmentFits)
                                {
                                    DateTime workingDateTime = currentCheckedDate.Add(currentStartTime);
                                    validAppointmentTimes.Add(workingDateTime);
                                }
                            }
                        }
                    }
                }
            }
            return validAppointmentTimes;
        }

        [Route("Home/Smartbox")]
        public ActionResult Smartbox()
        {
            QlabConsumer qlabConsumer = new QlabConsumer();
            var token = qlabConsumer.GetAccessToken().Result.AccessToken;
            var tokenEncoded = System.Web.HttpUtility.UrlEncode(token);

            ViewData["TokenUrl"] = "https://quote.smartboxmovingandstorage.com/?token=" + tokenEncoded.Trim();

            return View();
        }

        /// <summary>
        /// Helper method to find the number of days between two dates
        /// </summary>
        /// <param name="d1">First Date</param>
        /// <param name="d2">Second Date</param>
        /// <returns>Number of days between dates</returns>
        private int DaysBetween(DateTime d1, DateTime d2)
        {
            TimeSpan span = d2.Subtract(d1);
            return Math.Abs((int)span.TotalDays);
        }

        [HttpGet]
        [Route("/Denver")]
        public IActionResult Denver()
        {
            return RedirectToActionPermanent("Index");
        }
        [HttpGet]
        [Route("/Utah")]
        public IActionResult Utah()
        {
            return RedirectToActionPermanent("Index");
        }
        [HttpGet]
        [Route("/ColoradoSprings")]
        public IActionResult ColoradoSprings()
        {
            return RedirectToActionPermanent("Index");
        }
        [HttpGet]
        [Route("/>")]
        public IActionResult ClosedCarrot()
        {
            return RedirectToActionPermanent("Index");
        }
        [HttpGet]
        [Route("/wp-login.php")]
        [Route("/portal/wp-login.php")]
        [Route("/demo/wp-login.php")]
        [Route("/info/wp-login.php")]
        [Route("/old/wp-login.php")]
        [Route("/en/wp-login.php")]
        [Route("/sitio/wp-login.php")]
        [Route("/sites/wp-login.php")]
        [Route("/site/wp-login.php")]
        [Route("/news/wp-login.php")]
        [Route("/new/wp-login.php")]
        [Route("/web/wp-login.php")]
        [Route("/wp/wp-login.php")]
        [Route("/press/wp-login.php")]
        [Route("/wordpress/wp-login.php")]
        [Route(" /wp-content/db-cache.php")]
        [Route("/wordpress/wp-admin/")]
        [Route("/xmlrpc.php")]
        [Route("/magento_version")]
        [Route("/install.php")]
        [Route("/cgi/")]
        [Route("/banner/")]
        [Route("/admin/")]
        [Route("/backup/")]
        [Route("/beta/")]
        [Route("/banners/")]
        [Route("/backend/")]
        [Route("/apps/")]
        [Route("/assets/")]
        [Route("/.git/")]
        [Route("/.svn/")]
        public async Task<IActionResult> DamnBots()
        {
            await Task.Delay(10000);
            return StatusCode(403);
        }
        // Stop GoogleBots from requesting linked iOS apps
        [HttpGet]
        [Route("/apple-app-site-association")]
        [Route("/.well-known/assetlinks.json")]
        [Route("/.well-known/apple-app-site-association")]
        public IActionResult AppleSiteAssociation()
        {
            return StatusCode(403);
        }
    }
}