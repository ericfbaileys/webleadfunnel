﻿using Microsoft.EntityFrameworkCore;

namespace WebLeadFunnel.Models
{
    public class QlabDbContext : DbContext
    {
        public QlabDbContext(DbContextOptions<QlabDbContext> options):base(options)
        {

        }

        public DbSet<SalesPerson> SalesPeople { get; set; }
        public DbSet<SalesZip> SalesZips { get; set; }
    }
}
