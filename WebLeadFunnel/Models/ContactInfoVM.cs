﻿using System;

namespace WebLeadFunnel.Models
{
    public class ContactInfoVM
    {
        public string Steps { get; set; }
        public string ContactFormName { get; set; }
        public DateTime AppointmentLookupDay { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public string EstimatorName { get; set; }
        public string PromoCode { get; set; }
        public string Source { get; set; }
        public bool HomieChecked { get; set; }

        // Home
        public string OrigZipHouse { get; set; }
        public string DestZipHouse { get; set; }
        public DateTime? HouseDate { get; set; }
        public string HouseName { get; set; }
        public string HousePhone { get; set; }
        public string HouseEmail { get; set; }
        // Apartment
        public string OrigZipApt { get; set; }
        public string DestZipApt { get; set; }
        public DateTime? AptDate { get; set; }
        public string AptName { get; set; }
        public string AptPhone { get; set; }
        public string AptEmail { get; set; }
        // Street Address
        public string OriginAddress { get; set; }
        public string DestinationAddress { get; set; }
        // City/State from zip check
        public string OriginCity { get; set; }
        public string OriginState { get; set; }
        public string DestinationCity { get; set; }
        public string DestinationState { get; set; }

        // International
        public string IntlName { get; set; }
        public string IntlPhone { get; set; }
        public string IntlEmail { get; set; }
        // Other selections
        public string OtherWarehouseName { get; set; }
        public string OtherWarehousePhone { get; set; }
        public string OtherWarehouseEmail { get; set; }
        public string OtherSitName { get; set; }
        public string OtherSitPhone { get; set; }
        public string OtherSitEmail { get; set; }
        public string OtherSingleItemName { get; set; }
        public string OtherSingleItemPhone { get; set; }
        public string OtherSingleItemEmail { get; set; }
        // Office
        public string OfficeName { get; set; }
        public string OfficeCompanyName { get; set; }
        public string OfficePhone { get; set; }
        public string OfficeEmail { get; set; }
        public string OfficeTimeframe { get; set; }
        public string OfficeTellAboutProject { get; set; }

        // Helper properties
        public string OriginZip => Steps.Split(',')[0] == "House" ? OrigZipHouse : OrigZipApt;
        public string DestZip => Steps.Split(',')[0] == "House" ? DestZipHouse : DestZipApt;
        public DateTime? MoveDate
        {
            get
            {
                if (HouseDate != null || AptDate != null)
                {
                    if (Steps.Split(',')[0] == "House" || Steps.Split(',')[0] == "Apartment")
                    {
                        return Steps.Split(',')[0] == "House" ? HouseDate.Value : AptDate.Value;
                    }
                    return null;
                }
                return null;
            }
        }

        public string MoveDateFormatted => MoveDate.HasValue ? MoveDate.Value.ToShortDateString() : string.Empty;
        public string MoveType => Steps.Split(',')[0];
        public string MoveDistanceType => Steps.Split(',')?[2];
        public string MoveSize => Steps.Split(',')?[1];
        // Other option helpers
        private string OtherName => !string.IsNullOrEmpty(OtherWarehouseName)
            ? OtherWarehouseName
            : (!string.IsNullOrEmpty(OtherSitName)
                ? OtherSitName
                : (!string.IsNullOrEmpty(OtherSingleItemName)
                    ? OtherSingleItemName : string.Empty));
        private string OtherPhone => !string.IsNullOrEmpty(OtherWarehousePhone)
            ? OtherWarehousePhone
            : (!string.IsNullOrEmpty(OtherSitPhone)
                ? OtherSitPhone
                : (!string.IsNullOrEmpty(OtherSingleItemPhone)
                    ? OtherSingleItemPhone : string.Empty));
        private string OtherEmail => !string.IsNullOrEmpty(OtherWarehouseEmail)
            ? OtherWarehouseEmail
            : (!string.IsNullOrEmpty(OtherSitEmail)
                ? OtherSitEmail
                : (!string.IsNullOrEmpty(OtherSingleItemEmail)
                    ? OtherSingleItemEmail : string.Empty));

        // Helpers for storing info
        public string Name => !string.IsNullOrEmpty(HouseName)
            ? HouseName
            : (!string.IsNullOrEmpty(AptName)
                ? AptName
                : (!string.IsNullOrEmpty(IntlName)
                    ? IntlName
                    : (!string.IsNullOrEmpty(OfficeName) ? OfficeName
                        : (!string.IsNullOrEmpty(OtherName) ? OtherName : string.Empty))));

        public string Phone => !string.IsNullOrEmpty(HousePhone)
            ? HousePhone
            : (!string.IsNullOrEmpty(AptPhone)
                ? AptPhone
                : (!string.IsNullOrEmpty(IntlPhone)
                    ? IntlPhone
                    : (!string.IsNullOrEmpty(OfficePhone) ? OfficePhone
                        : (!string.IsNullOrEmpty(OtherPhone) ? OtherPhone : string.Empty))));

        public string Email => !string.IsNullOrEmpty(HouseEmail)
            ? HouseEmail
            : (!string.IsNullOrEmpty(AptEmail)
                ? AptEmail
                : (!string.IsNullOrEmpty(IntlEmail)
                    ? IntlEmail
                    : (!string.IsNullOrEmpty(OfficeEmail) ? OfficeEmail
                        : (!string.IsNullOrEmpty(OtherEmail) ? OtherEmail : string.Empty))));

        public ContactInfoVM() { }
    }
}
