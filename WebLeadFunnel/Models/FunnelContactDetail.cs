﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebLeadFunnel.Models
{
    public class FunnelContactDetail
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Type { get; set; }
        public string MoveType { get; set; }
        public string ContactFormName { get; set; }
        public string Steps { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int? OriginZip { get; set; }
        public int? DestinationZip { get; set; }
        public DateTime? MoveDate { get; set; }
        public string OfficeCompanyName { get; set; }
        public string OfficeTimeframe { get; set; }
        public string OfficeTellAboutProject { get; set; }
        public string OriginAddress { get; set; }
        public string DestinationAddress { get; set; }
        public string OriginCity { get; set; }
        public string OriginState { get; set; }
        public string DestinationCity { get; set; }
        public string DestinationState { get; set; }
        public int? LeadId { get; set; }
        public DateTime? LeadCreationDateTime { get; set; }
        public string SalesPersonName { get; set; }
        public string SalesPersonLocationCode { get; set; }
        public DateTime? AppointmentDateTime { get; set; }
        public string AppointmentType { get; set; }
        public int? OpportunityId { get; set; }
        public DateTime? OpportunityCreationDateTime { get; set; }
        public DateTime? SurveyAppointmentCreationDateTime { get; set; }
        public string PromoCode { get; set; }
        public string Source { get; set; }
    }
}
