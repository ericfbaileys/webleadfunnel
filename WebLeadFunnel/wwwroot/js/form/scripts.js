// https://stackoverflow.com/a/12302790
window.store = {
    localStoreSupport: function () {
        var testKey = 'test', storage = window.localStorage;
        try {
            storage.setItem(testKey, '1');
            storage.removeItem(testKey);
            return true;
        } catch (error) {
            return false;
        }
    },
    set: function (name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        if (this.localStoreSupport()) {
            localStorage.setItem(name, value);
        } else {
            document.cookie = name + "=" + value + expires + "; path=/";
        }
    },
    get: function (name) {
        if (this.localStoreSupport()) {
            var ret = localStorage.getItem(name);
            //console.log(typeof ret);
            switch (ret) {
                case 'true':
                    return true;
                case 'false':
                    return false;
                default:
                    return ret;
            }
        } else {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) === 0) {
                    ret = c.substring(nameEQ.length, c.length);
                    switch (ret) {
                        case 'true':
                            return true;
                        case 'false':
                            return false;
                        default:
                            return ret;
                    }
                }
            }
            return null;
        }
    },
    del: function (name) {
        if (this.localStoreSupport()) {
            localStorage.removeItem(name);
        } else {
            this.set(name, "", -1);
        }
    }
};

function nullJsonReplacer(i, val) {
    if (val === null) {
        return ""; // change null to empty string
    } else {
        return val; // return unchanged
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

$("#adalert").hide();

// Check for adblocker
function adBlockDetected() {
    $("#adalert").show();
    try {
        var trackerName = ga.getAll()[0].get('name');
        ga(trackerName + '.send', 'event', 'Funnel', 'Adblock Detected');
    } catch (err) {
        appInsights.trackException(err);
    }
    try {
        appInsights.trackEvent("Adblock Detected");
    } catch (err) {
        // continue regardless of error
    }
}
// Function called if AdBlock is not detected
function adBlockNotDetected() {
    $("#adalert").hide();
    console.log("no adblock");
}

// Recommended audit because AdBlock lock the file 'blockadblock.js' 
// If the file is not called, the variable does not exist 'blockAdBlock'
// This means that AdBlock is present
if (typeof blockAdBlock === 'undefined') {
    adBlockDetected();
} else {
    blockAdBlock.onDetected(adBlockDetected);
    blockAdBlock.onNotDetected(adBlockNotDetected);
}


// Try to prevent someone leaving the page
window.onbeforeunload = function () { return "Your work will be lost."; };
// Intercept Enter key and do nothing
$(document).keydown(function (e) {
    if (e.which === 13) {
        // enter pressed
    }
});

function redirectToHomieLanding(referralCode) {
    setTimeout(function() { window.location.href = `https://homie.com/moving?ref=${referralCode}`; }, 5000);
}

function triggerHomieRedirect() {
    var contactEmailAddress;
    if (store.get('steps').split(',')[0] === "House") {
        contactEmailAddress = $('#form-house-email-address').val();
    } else {
        contactEmailAddress = $('#form-apt-email-address').val();
    }

    redirectToHomieLanding(btoa(contactEmailAddress));
}

var longAjaxTimer; // Timer has to be declared globally
function CheckQuote(userData) {
    // Setup timer to display alternative text when the query is taking a while
    longAjaxTimer = setTimeout(function () { $('#estimationSpinnerDescription').text("This is taking longer than expected...to receive an exact quote from an estimator continue to the next screen"); }, 8000);

    var posting = $.post('/Home/Ballpark', JSON.stringify(userData, nullJsonReplacer));

    posting.done(function (data) {
        clearTimeout(longAjaxTimer);
        // Remove Spinner
        $("#estimationSpinner").hide();
        $('#estimationInfo').html(data);
    });
}

var longAjaxTimerLocal; // Timer has to be declared globally
function CheckLocalQuote(userData) {
    // Setup timer to display alternative text when the query is taking a while
    longAjaxTimerLocal = setTimeout(function () { $('#localEstimationSpinnerDescription').text("This is taking longer than expected...an error may have occurred."); }, 8000);

    var posting = $.post('/Home/LocalBallpark', JSON.stringify(userData, nullJsonReplacer));

    posting.done(function (data) {
        clearTimeout(longAjaxTimerLocal);
        // Remove Spinner
        $("#localEstimationSpinner").hide();
        $('#localEstimationInfo').html(data);
    });
}

// Called to tabulate and update Local 'Total Room Count' 
function UpdateLocalTotalRoomCount() {
    var localTotalRoomCount = 0;

    if (!isNaN(parseInt($('#form-local-bedroom-count').val()))) {
        localTotalRoomCount = parseInt(localTotalRoomCount) + parseInt($('#form-local-bedroom-count').val());
    }
    if (!isNaN(parseInt($('#form-local-bathroom-count').val()))) {
        localTotalRoomCount = parseInt(localTotalRoomCount) + parseInt($('#form-local-bathroom-count').val());
    }

    // Get number of checkboxes that are checked
    var checkboxesChecked = $('.localCheckboxGroup:checked');
    var numCheckboxes = parseInt(checkboxesChecked.length);
    // Check for extra other rooms
    if ($('#local-other-rooms-checkbox').prop('checked') === true) {
        if ($('#local-other-rooms-num').val() > 0) {
            numCheckboxes = parseInt(numCheckboxes) - 1; // Remove the check count from just selecting the option
            numCheckboxes = parseInt(numCheckboxes) + parseInt($('#local-other-rooms-num').val());
        }
    }

    if (!isNaN(parseInt(numCheckboxes))) {
        localTotalRoomCount = parseInt(localTotalRoomCount) + parseInt(numCheckboxes);
    }

    // Update value
    $('#form-local-total-room-count').val(localTotalRoomCount);
}


function VerifyZipDistance(zipData, next_page, parent_fieldset) {
    var posting = $.post('/Home/ZipCheck', JSON.stringify(zipData, nullJsonReplacer));
    posting.done(function (data) {
        // Init calendars

        var moveTypeCalendar = store.get('steps').split(',')[0];
        var calendarPicker;
        var weekendAlert;
        var distanceStep;
        var difficultiesAlert;
        if (moveTypeCalendar === "House") {
            calendarPicker = "#housedatepicker";
            weekendAlert = "#homeWeekendAlert";
            distanceStep = "housestep3";
            difficultiesAlert = "#homeDifficultiesAlert";
        } else {
            calendarPicker = "#apartmentdatepicker";
            weekendAlert = "#apartmentWeekendAlert";
            distanceStep = "apartmentstep3";
            difficultiesAlert = "#apartmentDifficultiesAlert";
        }

        //
        // Calender

        // Disable previous dates from being selected
        // Check which type of move they were interested in
        var moveDistance = store.get(distanceStep);
        var tomorrow = new Date();
        if (moveDistance === "Out of State (Across State Lines)") {
            tomorrow.setDate(tomorrow.getDate() + 10);
        } else {
            tomorrow.setDate(tomorrow.getDate() + 2);
        }
        $(calendarPicker).datetimepicker({
            inline: true,
            format: 'MM/dd/YYYY',
            minDate: tomorrow
        }).on('dp.change', function (e) {

            // Display the weekend rates warning when clicking a weekend date
            if (e.date.day() % 6 === 0) {
                $(weekendAlert).show();
            } else {
                $(weekendAlert).hide();
            }
            // Get the number of days between today and the one clicked
            var today = moment();
            var dayDifference = e.date.diff(today, 'days') + 2;

            if (moveDistance === "Local (Less than 50 miles)" && dayDifference <= 3) {
                $(difficultiesAlert).show();
            } else if (moveDistance === "In-State (More than 50 miles)" && dayDifference <= 10) {
                $(difficultiesAlert).show();
            } else {
                $(difficultiesAlert).hide();
            }
        });

        // Save Zip info
        store.set('zipInfo', JSON.stringify(data));
        // Display warning on calendars for local moves
        if (data.distance < 50) {
            $('#homeLocalAlert').show();
            $('#apartmentLocalAlert').show();
        }
        // Keep going to next page
        if (next_page !== undefined) {
            parent_fieldset.fadeOut(400,
                function () {
                    $('#' + next_page).fadeIn();
                });
        }
    });
}

function CheckAppointmentSuggestions(userData) {
    var posting = $.post('/Home/GetAvailableDatesAsync', JSON.stringify(userData));

    posting.done(function (data) {
        // Remove Spinner
        $("#appointmentSpinner").hide();
        $('#appointmentSuggestions').html(data);
    });

    posting.fail(function () {
        next_page = "wellcontact";
        if ($("#homie-checkbox-local").is(":checked") || $("#homie-checkbox-interstate").is(":checked")) {
            next_page = "wellcontactHomie";
        }
        $('#bookappointment').hide();
        $('#' + next_page).show();
        if ($("#homie-checkbox-local").is(":checked") || $("#homie-checkbox-interstate").is(":checked")) {
            triggerHomieRedirect();
        }
    });
}

function CollectInfo(userData, parent_fieldset) {
    var posting = $.post('/Home/CollectContactInfo', JSON.stringify(userData, this.nullJsonReplacer));

    posting.done(function (data) {
        // Send to we'll contact page
        parent_fieldset.fadeOut(400,
            function () {
                $('#wellcontact').fadeIn();
            });
    });
    try {
        var trackerName = ga.getAll()[0].get('name');
        ga(trackerName + '.send', 'event', 'Funnel', 'StorePotentialContactInfo');
        gtag_report_conversion();
        fbq('track', 'Lead');
    } catch (err) {
        appInsights.trackException(err);
    }
}

// Declare global variables
var parent_fieldset;
var current_page;
var next_page;

$(document).ready(function () {

    // Clear out previous steps on page load
    if (store.localStoreSupport) {
        store.del('steps');
        store.del('zipInfo');
        // Delete these values to remove international selection
        store.del('housestep3');
        store.del('apartmentstep3');
        store.del('estimatorInfo');
    }

    // Setup for jQuery GET and POST
    $.ajaxSetup({
        contentType: "application/json"
    });

    // Store promo code from url
    var promoCode = getParameterByName('promoCode');

    // Keep a variable to hold the estimator's info
    var estimatorInfo;
    
    /*
     *   Fullscreen background
     */
    $.backstretch([[{ url: "/images/backgrounds/1.jpg", alt: "Blue Background Image of Boxes" }]]);

    $('#top-navbar-1').on('shown.bs.collapse', function () {
        $.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function () {
        $.backstretch("resize");
    });

    /*
        Form
    */
    $('.registration-form fieldset:first-child').fadeIn('slow');

    $('.registration-form input[type="text"], .registration-form input[type="password"], .registration-form textarea').on('focus', function () {
        this.classList.remove("input-error");
    });

    // next step
    $('.registration-form .btn-form').on('click', function () {
        parent_fieldset = $(this).parents('fieldset');
        current_page = $(parent_fieldset).attr('id');

        // Get next step based on button clicked
        next_page = $(this).data('next');

        // Store the response that the user clicked
        var item_clicked = $(this).text();
        if (store.localStoreSupport) {
            store.set(current_page, item_clicked);

            // Add a new step to the step tracker
            if (store.get('steps') !== null) {
                var currentSteps = store.get('steps');
                store.set('steps', currentSteps + ',' + item_clicked);
            } else {
                store.set('steps', item_clicked);
            }
        }

        // Track click event
        try {
            var trackerName = ga.getAll()[0].get('name');
            ga(trackerName + '.send', 'event', 'Funnel', parent_fieldset.attr('id') + " - " + item_clicked);
        } catch (err) {
            appInsights.trackException(err);
        }
        try {
            appInsights.trackEvent("Funnel: " + parent_fieldset.attr('id') + " - " + item_clicked);
        } catch (err) {
            // continue regardless of error
        }

        // Verify inputs on form
        $(parent_fieldset).find('input[type="text"]').each(function () {
            // Check that there is a value except on the calendar pages
            if ($(this).val() === "") {
                next_page = undefined; // Stop the load of the next page
                this.classList.add("input-error");
            } else {
                // Since there is some value, remove the error but verify some further
                this.classList.remove("input-error");

                // Validate Name
                if ($(this).attr('name') === "form-name" || $(this).attr('name') === "form-apt-name") {
                    if (/(\w.+\s).+/.test($(this).val())) {
                        this.classList.remove("input-error");
                    } else {
                        next_page = undefined; // Stop the load of the next page
                        this.classList.add("input-error");
                    }
                }

                // Validate email
                if ($(this).data('validate') === "email") {
                    if (/@/.test($(this).val())) {
                        this.classList.remove("input-error");
                    } else {
                        next_page = undefined; // Stop the load of the next page
                        this.classList.add("input-error");
                    }
                }
                // Validate phone number
                if ($(this).data('validate') === "phone") {
                    if (/^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/.test($(this).val())) {
                        this.classList.remove("input-error");
                    } else {
                        next_page = undefined; // Stop the load of the next page
                        this.classList.add("input-error");
                    }
                }
                // Validate address
                if ($(this).data('validate') === "address") {
                    if (/^\d/.test($(this).val())) {
                        this.classList.remove("input-error");
                    } else {
                        next_page = undefined; // Stop the load of the next page
                        this.classList.add("input-error");
                    }
                }
            }
        });

        // Verify zip on form
        $(parent_fieldset).find('input[type="number"]').each(function () {
            if ($(this).val().length !== 5 && $(this).data('validate') === "zip") {
                next_page = undefined; // Stop the load of the next page
                this.classList.add("input-error");
            } else {
                this.classList.remove("input-error");
            }
        });

        // Send directly to get contact info if they selected a move type of International
        if (next_page !== undefined && (store.get('housestep3') === "International" || store.get('apartmentstep3') === "International")) {
            next_page = "intlcontact";
        }

        // Check to gather contact info and verify service area
        if (next_page === "ballparkorquote") {
            // Verify service area before sending to the ballparkorquote page
            // Get zip info
            var zipCheckInfoBp = store.get('zipInfo');
            zipCheckInfoBp = JSON.parse(zipCheckInfoBp);
            // Get data from form to send
            var ballparkData = {
                Steps: store.get('steps'),
                ContactFormName: "Home/Apt",
                MoveDistance: zipCheckInfoBp.distance,
                OrigZipHouse: $('#form-home-origin').val(),
                DestZipHouse: $('#form-home-destination').val(),
                HouseName: $('#form-house-name').val(),
                HousePhone: $('#form-house-contact-number').val(),
                HouseEmail: $('#form-house-email-address').val(),
                OrigZipApt: $('#form-apartment-origin').val(),
                DestZipApt: $('#form-apartment-destination').val(),
                AptName: $('#form-apt-name').val(),
                AptPhone: $('#form-apt-contact-number').val(),
                AptEmail: $('#form-apt-email-address').val(),
                OriginAddress: $('#form-origin-address').val(),
                DestinationAddress: $('#form-destination-address').val(),
                OriginCity: zipCheckInfoBp.originCity,
                OriginState: zipCheckInfoBp.originState,
                DestinationCity: zipCheckInfoBp.destinationCity,
                DestinationState: zipCheckInfoBp.destinationState,
                PromoCode: promoCode,
                Source: "Web"
            };
            // Get calendar date based on type of move selected
            if (ballparkData.Steps.split(',')[0] === "House") {
                ballparkData.HouseDate = $('#housedatepicker').data("DateTimePicker").date();
            } else {
                ballparkData.AptDate = $('#apartmentdatepicker').data("DateTimePicker").date();
            }

            // Add a spinner to signify that the date is being processed
            $('[data-next="ballparkorquote"]').html('<img class="center-block" src="/images/ajax-loader.gif" style="width: 1.5em;" />');


            var verifyServiceAreaRequest = $.post('/Home/VerifyServiceArea', JSON.stringify(ballparkData, this.nullJsonReplacer));
            verifyServiceAreaRequest.done(function (data) {
                estimatorInfo = data;
                store.set('estimatorInfo', JSON.stringify(data));
                next_page = "ballparkorquote";

                // Check distance to see if the move is actually local
                var zipInfo = store.get('zipInfo');
                zipInfo = JSON.parse(zipInfo);

                // If the distance is less than 50 miles, send to Local ballpark screen
                if (zipInfo.distance < 50) {

                    // Check that this user is in our service area before offering a local quote
                    if (estimatorInfo.responseType === "origin" || estimatorInfo.responseType === "both") {
                        next_page = "localballpark";
                    }

                    // Pre-Populate bedroom count
                    var bedroomSelection = ballparkData.Steps.split(',')[1];

                    switch (bedroomSelection) {
                        case '1 Bedroom':
                            $("#form-local-bedroom-count").val(1); break;
                        case '2 Bedrooms':
                            $("#form-local-bedroom-count").val(2); break;
                        case '3 Bedrooms':
                            $("#form-local-bedroom-count").val(3); break;
                        case '4+ Bedrooms':
                            $("#form-local-bedroom-count").val(4); break;
                    }

                    UpdateLocalTotalRoomCount();
                }

                // Send right to the 'We will contact' page
                // if they aren't in our service area
                if (next_page !== undefined && next_page !== "localballpark" && (estimatorInfo.responseType === "neither" || estimatorInfo.responseType === "destination")) {
                    // Collect info and send to we'll contact page
                    CollectInfo(ballparkData, parent_fieldset);
                    next_page = "wellcontact";
                }
                if (next_page !== undefined) {
                    parent_fieldset.fadeOut(400,
                        function () {
                            $('#' + next_page).fadeIn();
                        });
                }
                try {
                    var trackerName = ga.getAll()[0].get('name');
                    ga(trackerName + '.send', 'event', 'Funnel', 'StorePotentialContactInfo');
                    fbq('track', 'Lead');
                } catch (err) {
                    appInsights.trackException(err);
                }
            });
            verifyServiceAreaRequest.fail(function (data) {
                CollectInfo(ballparkData, parent_fieldset);
                next_page = "wellcontact";
                parent_fieldset.fadeOut(400,
                    function () {
                        $('#' + next_page).fadeIn();
                    });
            });

        } else {
            //
            // Other panel handling
            //

            // Trigger Homie Email send
            if (next_page === "gatherAddress" && (current_page === "localballparkresults" || current_page === "estimater")) {
                // Look at checkbox status to determine if Homie email is triggered
                if ($("#homie-checkbox-local").is(":checked") || $("#homie-checkbox-interstate").is(":checked")) {
                    // Get zip info
                    var zipCheckInfoHomie = store.get('zipInfo');
                    zipCheckInfoHomie = JSON.parse(zipCheckInfoHomie);
                    // Get data from form to send
                    var homieData = {
                        Steps: store.get('steps'),
                        ContactFormName: "Home/Apt",
                        MoveDistance: zipCheckInfoHomie.distance,
                        OrigZipHouse: $('#form-home-origin').val(),
                        DestZipHouse: $('#form-home-destination').val(),
                        HouseName: $('#form-house-name').val(),
                        HousePhone: $('#form-house-contact-number').val(),
                        HouseEmail: $('#form-house-email-address').val(),
                        OrigZipApt: $('#form-apartment-origin').val(),
                        DestZipApt: $('#form-apartment-destination').val(),
                        AptName: $('#form-apt-name').val(),
                        AptPhone: $('#form-apt-contact-number').val(),
                        AptEmail: $('#form-apt-email-address').val(),
                        OriginAddress: $('#form-origin-address').val(),
                        DestinationAddress: $('#form-destination-address').val(),
                        OriginCity: zipCheckInfoHomie.originCity,
                        OriginState: zipCheckInfoHomie.originState,
                        DestinationCity: zipCheckInfoHomie.destinationCity,
                        DestinationState: zipCheckInfoHomie.destinationState,
                        PromoCode: promoCode,
                        Source: "Web"
                    };
                    // Get calendar date based on type of move selected
                    if (homieData.Steps.split(',')[0] === "House") {
                        homieData.HouseDate = $('#housedatepicker').data("DateTimePicker").date();
                    } else {
                        homieData.AptDate = $('#apartmentdatepicker').data("DateTimePicker").date();
                    }

                    var homieInterestResponse = $.post('/Home/CollectHomieRequest', JSON.stringify(homieData, this.nullJsonReplacer));
                }
            }

            
            // Check to see about presenting Yembo page
            if (next_page === "gatherAddress" && current_page !== "yemboPrompt") {
                // Check that the state is CO and that it's a true local (less than 50 miles)
                // Get zip info
                var zipCheckInfoYembo = store.get('zipInfo');
                zipCheckInfoYembo = JSON.parse(zipCheckInfoYembo);

                // True local check and State match
                if (zipCheckInfoYembo.distance < 50 && zipCheckInfoYembo.originState === "CO" && zipCheckInfoYembo.destinationState === "CO") {
                    // Present the Yembo page
                    next_page = "yemboPrompt";
                }
            }

            // Trigger Yembo workflow
            if (next_page === "yemboConfirmed") {
                // Get zip info
                var zipCheckInfoYemboConfirmed = store.get('zipInfo');
                zipCheckInfoYemboConfirmed = JSON.parse(zipCheckInfoYemboConfirmed);
                // Get data from form to send
                var yemboData = {
                    Steps: store.get('steps'),
                    ContactFormName: "Home/Apt",
                    MoveDistance: zipCheckInfoYemboConfirmed.distance,
                    OrigZipHouse: $('#form-home-origin').val(),
                    DestZipHouse: $('#form-home-destination').val(),
                    HouseName: $('#form-house-name').val(),
                    HousePhone: $('#form-house-contact-number').val(),
                    HouseEmail: $('#form-house-email-address').val(),
                    OrigZipApt: $('#form-apartment-origin').val(),
                    DestZipApt: $('#form-apartment-destination').val(),
                    AptName: $('#form-apt-name').val(),
                    AptPhone: $('#form-apt-contact-number').val(),
                    AptEmail: $('#form-apt-email-address').val(),
                    OriginAddress: $('#form-origin-address').val(),
                    DestinationAddress: $('#form-destination-address').val(),
                    OriginCity: zipCheckInfoYemboConfirmed.originCity,
                    OriginState: zipCheckInfoYemboConfirmed.originState,
                    DestinationCity: zipCheckInfoYemboConfirmed.destinationCity,
                    DestinationState: zipCheckInfoYemboConfirmed.destinationState,
                    PromoCode: promoCode,
                    Source: "Web"
                };
                // Get calendar date based on type of move selected
                if (yemboData.Steps.split(',')[0] === "House") {
                    yemboData.HouseDate = $('#housedatepicker').data("DateTimePicker").date();
                } else {
                    yemboData.AptDate = $('#apartmentdatepicker').data("DateTimePicker").date();
                }

                var yemboConfirmResponse = $.post('/Home/CollectYemboRequest', JSON.stringify(yemboData, this.nullJsonReplacer));
                
                // Send to the We'll Contact page
                next_page = "wellcontactYembo";
            }

            // Trigger Allied Estimator before page loads
            if (next_page === "estimater") {
                // Get zip info
                var zipCheckInfoEstimator = store.get('zipInfo');
                zipCheckInfoEstimator = JSON.parse(zipCheckInfoEstimator);

                // Get data from form to send
                var quoteData = {
                    Steps: store.get('steps'),
                    OrigZipHouse: $('#form-home-origin').val(),
                    DestZipHouse: $('#form-home-destination').val(),
                    HouseName: $('#form-house-name').val(),
                    HousePhone: $('#form-house-contact-number').val(),
                    HouseEmail: $('#form-house-email-address').val(),
                    OrigZipApt: $('#form-apartment-origin').val(),
                    DestZipApt: $('#form-apartment-destination').val(),
                    AptName: $('#form-apt-name').val(),
                    AptPhone: $('#form-apt-contact-number').val(),
                    AptEmail: $('#form-apt-email-address').val(),
                    OriginCity: zipCheckInfoEstimator.originCity,
                    OriginState: zipCheckInfoEstimator.originState,
                    DestinationCity: zipCheckInfoEstimator.destinationCity,
                    DestinationState: zipCheckInfoEstimator.destinationState,
                    PromoCode: promoCode,
                    Source: "Web"
                };
                // Get calendar date based on type of move selected
                if (quoteData.Steps.split(',')[0] === "House") {
                    quoteData.HouseDate = $('#housedatepicker').data("DateTimePicker").date();
                } else {
                    quoteData.AptDate = $('#apartmentdatepicker').data("DateTimePicker").date();
                }

                // Call estimator to update estimation page
                CheckQuote(quoteData);
            }
            
            // Trigger Appointment Lookup before page loads
            if (next_page === "bookappointment") {
                // Get estimator info
                var estimatorInfo = store.get('estimatorInfo');
                estimatorInfo = JSON.parse(estimatorInfo);
                // Call appointment checker to update available appointments
                CheckAppointmentSuggestions(estimatorInfo);
            }

            // Lookup local move before the page loads
            if (next_page === "localballparkresults") {
                // Get number of checkboxes that are checked
                var checkboxesChecked = $('.localCheckboxGroup:checked');
                var numCheckboxes = parseInt(checkboxesChecked.length);
                // Check for extra other rooms
                if ($('#local-other-rooms-checkbox').prop('checked') === true) {
                    if ($('#local-other-rooms-num').val() > 0) {
                        numCheckboxes = parseInt(numCheckboxes) - 1; // Remove the check count from just selecting the option
                        numCheckboxes = parseInt(numCheckboxes) + parseInt($('#local-other-rooms-num').val());
                    }
                }

                var localOriginZip;
                var localDestinationZip;
                var localMoveDate;
                if (store.get('steps').split(',')[0] === "House") {
                    localOriginZip = $('#form-home-origin').val();
                    localDestinationZip = $('#form-home-destination').val();
                    localMoveDate = $('#housedatepicker').data("DateTimePicker").date();
                } else {
                    localOriginZip = $('#form-apartment-origin').val();
                    localDestinationZip = $('#form-apartment-destination').val();
                    localMoveDate = $('#apartmentdatepicker').data("DateTimePicker").date();
                }

                // Create request to get local move quote
                var localQuoteData = {
                    OriginZip: localOriginZip,
                    DestinationZip: localDestinationZip,
                    MoveDate: localMoveDate,
                    HomeSize: store.get('steps').split(',')[1],
                    OtherRooms: parseInt(numCheckboxes)
                };

                CheckLocalQuote(localQuoteData);
            }

            // Calculate the zip distance to make sure it's not a local
            if (next_page === "housestep5" || next_page === "apartmentstep5") {
                var temp_next = next_page;
                next_page = undefined;
                var zipData = {
                    OrigZipHouse: $('#form-home-origin').val(),
                    DestZipHouse: $('#form-home-destination').val(),
                    OrigZipApt: $('#form-apartment-origin').val(),
                    DestZipApt: $('#form-apartment-destination').val()
                };

                // Add a spinner to signify that the date is being processed
                $('[data-next="housestep5"]').html('<img class="center-block" src="/images/ajax-loader.gif" style="width: 1.5em;" />');
                $('[data-next="apartmentstep5"]').html('<img class="center-block" src="/images/ajax-loader.gif" style="width: 1.5em;" />');

                VerifyZipDistance(zipData, temp_next, parent_fieldset);
            }

            // Load in assumed appointment calendar right before it's needed
            if (next_page === 'assumedApptCalendar') {
                //
                // Calendar for Assumed Appointments
                // Calculate days out to block those dates from being selected

                // Get estimator info
                var estimatorInfoAssumed = store.get('estimatorInfo');
                estimatorInfoAssumed = JSON.parse(estimatorInfoAssumed);
                var daysOutDate = new Date();
                daysOutDate.setDate(daysOutDate.getDate() + parseInt(estimatorInfoAssumed.scheduleDaysOut) + 1);
                $('#assumedAppointmentdatepicker').datetimepicker({
                    inline: true,
                    sideBySide: true,
                    stepping: 30,
                    daysOfWeekDisabled: [0, 6],
                    minDate: daysOutDate // needs to limit days out for scheduling
                });
            }

            // Change contact page if Homie contact was selected
            if (next_page !== undefined && next_page.startsWith("wellcontact")) {
                if ($("#homie-checkbox-local").is(":checked") || $("#homie-checkbox-interstate").is(":checked")) {
                    next_page = "wellcontactHomie";
                }
            }

            // Send contact info if it's that type of page
            if ($(parent_fieldset).data('contactformname') !== null && $(parent_fieldset).data('contactformname') !== undefined && next_page !== undefined) {
                // Get zip info
                var zipCheckInfo = store.get('zipInfo');
                zipCheckInfo = JSON.parse(zipCheckInfo);
                // Get data from form to send
                var contactData = {
                    Steps: store.get('steps'),
                    ContactFormName: $(parent_fieldset).data('contactformname'),
                    OrigZipHouse: parseInt($('#form-home-origin').val()),
                    DestZipHouse: parseInt($('#form-home-destination').val()),
                    HouseName: $('#form-house-name').val(),
                    HousePhone: $('#form-house-contact-number').val(),
                    HouseEmail: $('#form-house-email-address').val(),
                    OrigZipApt: parseInt($('#form-apartment-origin').val()),
                    DestZipApt: parseInt($('#form-apartment-destination').val()),
                    AptName: $('#form-apt-name').val(),
                    AptPhone: $('#form-apt-contact-number').val(),
                    AptEmail: $('#form-apt-email-address').val(),
                    OriginAddress: $('#form-origin-address').val(),
                    DestinationAddress: $('#form-destination-address').val(),
                    IntlName: $('#form-intl-name').val(),
                    IntlPhone: $('#form-house-intl-number').val(),
                    IntlEmail: $('#form-house-intl-address').val(),
                    OfficeName: $('#form-office-name').val(),
                    OfficeCompanyName: $('#form-office-company').val(),
                    OfficePhone: $('#form-office-contact-number').val(),
                    OfficeEmail: $('#form-office-email-address').val(),
                    OfficeTimeframe: $('#form-office-timeframe').val(),
                    OfficeTellAboutProject: $('#form-office-about').val(),
                    OtherWarehouseName: $('#form-warehouse-name').val(),
                    OtherWarehousePhone: $('#form-warehouse-contact-number').val(),
                    OtherWarehouseEmail: $('#form-warehouse-email-address').val(),
                    OtherSitName: $('#form-sit-name').val(),
                    OtherSitPhone: $('#form-sit-contact-number').val(),
                    OtherSitEmail: $('#form-sit-email-address').val(),
                    OtherSingleItemName: $('#form-singleitem-name').val(),
                    OtherSingleItemPhone: $('#form-singleitem-contact-number').val(),
                    OtherSingleItemEmail: $('#form-singleitem-email-address').val(),
                    PromoCode: promoCode,
                    Source: "Web"
                };
                // Don't grab date if it's an international
                if (contactData.ContactFormName !== "International" && contactData.ContactFormName !== "Office") {
                    // Get calendar date based on type of move selected
                    if (contactData.Steps.split(',')[0] === "House") {
                        contactData.HouseDate =
                            $('#housedatepicker').data("DateTimePicker").date().format("MM-DD-YYYY");
                    }
                    if (contactData.Steps.split(',')[0] === "Apartment") {
                        contactData.AptDate =
                            $('#apartmentdatepicker').data("DateTimePicker").date().format("MM-DD-YYYY");
                    }
                }

                if (zipCheckInfo !== null) {
                    contactData.OriginCity = zipCheckInfo.originCity;
                    contactData.OriginState = zipCheckInfo.originState;
                    contactData.DestinationCity = zipCheckInfo.destinationCity;
                    contactData.DestinationState = zipCheckInfo.destinationState;
                }

                CollectInfo(contactData, parent_fieldset);
                next_page = undefined;
            }
            

            // https://homie.com/moving?ref=
            // Trigger Homie redirect timer
            if (next_page === "wellcontactHomie") {
                triggerHomieRedirect();
            }

            // Standard continue through the pages
            if (next_page !== undefined) {

                // Smartbox redirect
                if (next_page === 'smartboxstep3') {
                    window.location = "http://" + window.location.hostname + "/home/smartbox";
                    //window.location = "http://www.smartboxmovingandstorage.com/free-quote";
                }

                parent_fieldset.fadeOut(400,
                    function () {
                        $('#' + next_page).fadeIn();
                    });
            }
        }

    });

    // Only allow input to local 'Other rooms' textbox when option is checked
    $('#local-other-rooms-checkbox').change(function () {
        if (this.checked) {
            // When initially checking the 'Other rooms' checkbox, add a value
            if (isNaN(parseInt($('#local-other-rooms-num').val()))) {
                $('#local-other-rooms-num').val(1);
            }
            $('#local-other-rooms-num').prop('disabled', false);
        } else {
            $('#local-other-rooms-num').val("");
            $('#local-other-rooms-num').prop('disabled', true);
        }
    });

    // Listen to changes in the local move 'Other rooms' textbox
    $('#local-other-rooms-num').on('input',
        function () {
            if ($('#local-other-rooms-num').val().length !== 0 && $('#local-other-rooms-num').val() > 0) {
                // Make sure that the checkbox is checked if there is a value
                $('#local-other-rooms-checkbox').prop("checked", true);
            } else {
                // Remove checkbox when there is no value
                $('#local-other-rooms-checkbox').prop("checked", false);
            }
        });

    // Register handlers to update local 'Total Room Count'
    $('#form-local-bedroom-count').change(function () {
        UpdateLocalTotalRoomCount();
    });
    $('#form-local-bathroom-count').change(function () {
        UpdateLocalTotalRoomCount();
    });
    $('.localCheckboxGroup').change(function () {
        UpdateLocalTotalRoomCount();
    });
    $('#local-other-rooms-num').on('input', function () {
        UpdateLocalTotalRoomCount();
    });

    // Hide weekend rate display
    $('#homeWeekendAlert').hide();
    // Hide the difficulty display
    $('#homeDifficultiesAlert').hide();
    // Hide local move alert
    $('#homeLocalAlert').hide();

    // Hide weekend rate display
    $('#apartmentWeekendAlert').hide();
    // Hide the difficulty display
    $('#apartmentDifficultiesAlert').hide();
    // Hide local move alert
    $('#apartmentLocalAlert').hide();

    //
    // Calendar for viewing all appointment slots

    // Calculate days out to block those dates from being selected
    var daysOutDate = new Date();
    daysOutDate.setDate(daysOutDate.getDate());
    $('#appointmentdatepicker').datetimepicker({
        inline: true,
        format: 'MM/dd/YYYY',
        daysOfWeekDisabled: [0, 6],
        minDate: daysOutDate // needs to limit days out for scheduling
    }).on('dp.change', function (e) {
        // Replace the list of dates with a spinner
        $('#appointmentCalendarSuggestions').html("<img class='center-block' src='/images/ajax-loader.gif' style='width: 1.5em;' /><p class='text-center'>Please wait while we retrieve open appointment time slots</p >");

        // Get estimator info
        var estimatorInfo = store.get('estimatorInfo');
        estimatorInfo = JSON.parse(estimatorInfo);
        // Add selected date
        var selectedDate = e.date;
        estimatorInfo.appointmentLookupDate = selectedDate.format("YYYY-MM-DD");

        // Lookup available appointments for the selected date
        var posting = $.post('/Home/GetAvailableDatesAsync', JSON.stringify(estimatorInfo));

        posting.done(function (data) {
            $('#appointmentCalendarSuggestions').html(data);
            $('#needAptNotListed').show();
        });
    });

    // Hide appointment not listed button
    $('#needAptNotListed').hide();

    // Handle confirmation of assumed appointment
    $('#assumedApptConfirm').confirmation({
        rootSelector: '#assumedApptConfirm',
        popout: true,
        title: "Are you sure you want to confirm this appointment time?",
        placement: "top",
        btnOkClass: "btn-sm btn-success",
        btnCancelClass: "btn-sm btn-default",
        onConfirm: function () {
            // Get estimator info
            var estimatorInfo = store.get('estimatorInfo');
            estimatorInfo = JSON.parse(estimatorInfo);
            // Get zip info
            var zipCheckInfo = store.get('zipInfo');
            zipCheckInfo = JSON.parse(zipCheckInfo);
            
            // Date assumed appointment date/time from control
            var assumedAptContactData = {
                Steps: store.get('steps'),
                ContactFormName: 'assumedApptCalendar',
                AppointmentDateTime: $('#assumedAppointmentdatepicker').data("DateTimePicker").date(),
                EstimatorName: typeof estimatorInfo.estimatorName == "undefined" || estimatorInfo.estimatorName == null || estimatorInfo.estimatorName === '' ? "unassigned estimator" : estimatorInfo.estimatorName,
                OrigZipHouse: $('#form-home-origin').val(),
                DestZipHouse: $('#form-home-destination').val(),
                HouseName: $('#form-house-name').val(),
                HousePhone: $('#form-house-contact-number').val(),
                HouseEmail: $('#form-house-email-address').val(),
                OriginZipApt: $('#form-apartment-origin').val(),
                DestZipApt: $('#form-apartment-destination').val(),
                AptName: $('#form-apt-name').val(),
                AptPhone: $('#form-apt-contact-number').val(),
                AptEmail: $('#form-apt-email-address').val(),
                OriginAddress: $('#form-origin-address').val(),
                DestinationAddress: $('#form-destination-address').val(),
                IntlName: $('#form-intl-name').val(),
                IntlPhone: $('#form-house-intl-number').val(),
                IntlEmail: $('#form-house-intl-address').val(),
                OfficeName: $('#form-office-name').val(),
                OfficeCompanyName: $('#form-office-company').val(),
                OfficePhone: $('#form-office-contact-number').val(),
                OfficeEmail: $('#form-office-email-address').val(),
                OfficeTimeframe: $('#form-office-timeframe').val(),
                OfficeTellAboutProject: $('#form-office-about').val(),
                OtherWarehouseName: $('#form-warehouse-name').val(),
                OtherWarehousePhone: $('#form-warehouse-contact-number').val(),
                OtherWarehouseEmail: $('#form-warehouse-email-address').val(),
                OtherSitName: $('#form-sit-name').val(),
                OtherSitPhone: $('#form-sit-contact-number').val(),
                OtherSitEmail: $('#form-sit-email-address').val(),
                OtherSingleItemName: $('#form-singleitem-name').val(),
                OtherSingleItemPhone: $('#form-singleitem-contact-number').val(),
                OtherSingleItemEmail: $('#form-singleitem-email-address').val(),
                OriginCity: zipCheckInfo.originCity,
                OriginState: zipCheckInfo.originState,
                DestinationCity: zipCheckInfo.destinationCity,
                DestinationState: zipCheckInfo.destinationState,
                PromoCode: promoCode,
                Source: "Web"
            };
            // Get calendar date based on type of move selected
            if (assumedAptContactData.Steps.split(',')[0] === "House") {
                assumedAptContactData.HouseDate = $('#housedatepicker').data("DateTimePicker").date();
            } else {
                assumedAptContactData.AptDate = $('#apartmentdatepicker').data("DateTimePicker").date();
            }

            // Handle Homie checkbox
            if (zipCheckInfo.distance < 50) {
                assumedAptContactData.HomieChecked = $('#homie-checkbox-local').is(":checked");
            } else {
                assumedAptContactData.HomieChecked = $('#homie-checkbox-interstate').is(":checked");
            }

            var posting = $.post('/Home/CollectAppointmentData', JSON.stringify(assumedAptContactData, nullJsonReplacer));

            posting.done(function (data) {
                // Send to we'll contact page
                $('#assumedApptCalendar').fadeOut(400,
                    function () {
                        $('#wellcontact').fadeIn();
                    });
            });

            // Send tracking trigger
            var moveType = store.get('steps').split(',')[0];
            if (moveType === "House") {
                var moveDistance = store.get('housestep3');
                var triggerEventName = moveType + "|" + moveDistance + "|assumed-appointment";
                triggerEventName = triggerEventName.replace(" ", "_");
                console.log(triggerEventName);
                dataLayer.push({ 'event': triggerEventName });
            } else {
                var moveDistanceApt = store.get('apartmentstep3');
                var triggerEventNameApt = moveType + "|" + moveDistanceApt + "|assumed-appointment";
                triggerEventNameApt = triggerEventNameApt.replace(" ", "_");
                console.log(triggerEventNameApt);
                dataLayer.push({ 'event': triggerEventNameApt });
            }
        }
    });

    /*
     * Click handler to trigger Homie checkbox on it's button
     */
    $('#homie-button-local').off('click').on('click',
        function (e) {
            e.preventDefault();
            $('#homie-checkbox-local').is(':checked') ? $('#homie-checkbox-local').prop('checked', false) : $('#homie-checkbox-local').prop('checked', true);
        });
    $('#homie-checkbox-local').on('click',
        function(e) {
            e.stopPropagation();
        });
    $('#homie-button-interstate').off('click').on('click',
        function (e) {
            e.preventDefault();
            $('#homie-checkbox-interstate').is(':checked') ? $('#homie-checkbox-interstate').prop('checked', false) : $('#homie-checkbox-interstate').prop('checked', true);
        });
    $('#homie-checkbox-interstate').on('click',
        function (e) {
            e.stopPropagation();
        });
});
