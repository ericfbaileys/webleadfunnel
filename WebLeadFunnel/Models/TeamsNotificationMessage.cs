﻿using System;

namespace WebLeadFunnel.Models
{
    public class TeamsNotificationMessage
    {
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string ApptTime { get; set; }
        public string Estimator { get; set; }
        public int OriginZip { get; set; }
        public int DestinationZip { get; set; }
    }
}
