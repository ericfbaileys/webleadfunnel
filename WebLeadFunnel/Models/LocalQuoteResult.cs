﻿using Newtonsoft.Json;

namespace WebLeadFunnel.Models
{
    public class LocalQuoteResult
    {
        [JsonProperty("High")]
        public LocalQuoteDetails High { get; set; }

        [JsonProperty("Mid")]
        public LocalQuoteDetails Mid { get; set; }

        [JsonProperty("Low")]
        public LocalQuoteDetails Low { get; set; }
    }

    public class LocalQuoteDetails
    {
        [JsonProperty("Rate")]
        public string Rate { get; set; }

        [JsonProperty("CrewSize")]
        public string CrewSize { get; set; }

        [JsonProperty("JobHours")]
        public string JobHours { get; set; }
    }
}
