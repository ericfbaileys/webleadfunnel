﻿using System.Collections.Generic;

namespace WebLeadFunnel.Models
{
    public class SalesPerson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public int ScheduleDaysOut { get; set; }
        public int ScheduleHoursApart { get; set; }
        public int ScheduleMinutesApart { get; set; }
        public string CalendarId { get; set; }
        public string LocationCode { get; set; }
        public List<SalesZip> ZipCodes { get; set; }
        public List<SalesScheduleAvailibility> Availibilities { get; set; }

        public SalesPerson()
        {
        }
    }
}
