﻿using System;
using Newtonsoft.Json;

namespace WebLeadFunnel.Data
{
    public class Token
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("expires_in")]
        public DateTime Expiration { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        public Token() { }

        [JsonConstructor]
        public Token(string access_token, string expires_in, string token_type)
        {
            AccessToken = access_token;
            Expiration = DateTime.Now.AddSeconds(int.Parse(expires_in));
            TokenType = token_type;
        }
    }
}
