﻿namespace WebLeadFunnel.Models
{
    public class LocationLookupResult
    {
        public double Distance { get; set; }
        public string OriginCity { get; set; }
        public string OriginState { get; set; }
        public string DestinationCity { get; set; }
        public string DestinationState { get; set; }
    }
}
