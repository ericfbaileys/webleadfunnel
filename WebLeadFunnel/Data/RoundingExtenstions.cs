﻿using System;

namespace WebLeadFunnel.Data
{
    public static class RoundingExtenstions
    {

        public static int RoundOffto10(this int i)
        {
            return ((int)Math.Round(i / 10.0)) * 10;
        }

        public static decimal RoundOffto10(this decimal i)
        {
            return ((int)Math.Round(Math.Round(i) / (decimal)10.0)) * 10;
        }

        public static decimal RoundOffto25(this decimal i)
        {
            int roundingResult = ((int) Math.Round(Math.Round(i) / (decimal) 25.0)) * 25;
            if (roundingResult < Math.Ceiling(i))
            {
                // Always round up
                roundingResult = roundingResult + 25;
                return roundingResult;
            } 
            return roundingResult;
        }

        public static decimal AddComissionAndRoundOffto10(this decimal i)
        {
            decimal comissionValue = i * (decimal)0.15;
            if (comissionValue < 350)
            {
                comissionValue = 350;
            }
            i += comissionValue;
            return ((int)Math.Round(Math.Round(i) / (decimal)10.0)) * 10;
        }
    }

}
