﻿using BaileysNuget.YemboApiService;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using BaileysNuget.YemboApiService.Models;
using WebLeadFunnel.Models;
using BinaryFog.NameParser;

namespace WebLeadFunnel.Controllers
{
    public class YemboController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public YemboController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<IActionResult> Homie(ContactInfoVM contactInfo)
        {
            HttpClient yemboHttpClient = _httpClientFactory.CreateClient();
            SecretClient keyvaultSecretClient = new SecretClient(new Uri(Environment.GetEnvironmentVariable("BaileysInternalVaultUri")), new DefaultAzureCredential());
            YemboApiService yemboApiService = new YemboApiService(yemboHttpClient, keyvaultSecretClient);

            // Split up Name
            FullNameParser fullNameParser = new FullNameParser(contactInfo.Name);
            fullNameParser.Parse();

            YemboCustomerInfo customerInfo = new YemboCustomerInfo
            {
                Consumer = new Consumer()
                {
                    GivenName = fullNameParser.FirstName,
                    FamilyName = fullNameParser.LastName,
                    Email = contactInfo.Email,
                    Phone = contactInfo.Phone
                }
            };
            // Check for Move Date
            if (contactInfo.MoveDate.HasValue)
            {
                customerInfo.MoveInfo = new MoveInfo()
                {
                    Date = contactInfo.MoveDate.Value,
                    DateKnown = true
                };
            }

            YemboInitParamResponse yemboResponse = await yemboApiService.PopulateBackendInformation(customerInfo);
            string movekey = yemboResponse.MoveKey;


            // Send Invite request
            YemboInviteRequest inviteRequest = new YemboInviteRequest
            {
                Email = customerInfo.Consumer.Email,
                GivenName = customerInfo.Consumer.GivenName,
                FamilyName = customerInfo.Consumer.FamilyName,
                Phone = customerInfo.Consumer.Phone,
                Identifier = yemboResponse.Identifier,
                MoveKey = yemboResponse.MoveKey
            };

            var inviteResponse = await yemboApiService.SendInviteToCustomer(inviteRequest);

            return Content(movekey);
        }
        public async Task<IActionResult> HomieTest()
        {
            HttpClient yemboHttpClient = _httpClientFactory.CreateClient();
            SecretClient keyvaultSecretClient = new SecretClient(new Uri(Environment.GetEnvironmentVariable("BaileysInternalVaultUri")), new DefaultAzureCredential());
            YemboApiService yemboApiService = new YemboApiService(yemboHttpClient, keyvaultSecretClient);


            YemboCustomerInfo customerInfo = new YemboCustomerInfo();
            customerInfo.Consumer = new Consumer()
                {GivenName = "Tester", FamilyName = "McTesterson 6", Email = "test@testtest.com", Phone = "555-555-5555"};
            customerInfo.MoveInfo = new MoveInfo()
            {
                Date = DateTime.Parse("2020-1-4"),
                DateKnown = true
            };

            YemboInitParamResponse yemboResponse = await yemboApiService.PopulateBackendInformation(customerInfo);
            string movekey = yemboResponse.MoveKey;


            // Send Invite request
            YemboInviteRequest inviteRequest = new YemboInviteRequest();
            inviteRequest.Email = customerInfo.Consumer.Email;
            inviteRequest.GivenName = customerInfo.Consumer.GivenName;
            inviteRequest.FamilyName = customerInfo.Consumer.FamilyName;
            inviteRequest.Phone = customerInfo.Consumer.Phone;

            inviteRequest.Identifier = yemboResponse.Identifier;
            inviteRequest.MoveKey = yemboResponse.MoveKey;

            var inviteResponse = await yemboApiService.SendInviteToCustomer(inviteRequest);

            return Content(movekey);
        }
    }
}
