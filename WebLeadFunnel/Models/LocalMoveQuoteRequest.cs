﻿namespace WebLeadFunnel.Models
{
    public class LocalMoveQuoteRequest
    {
        public string OriginZip { get; set; }
        public string DestinationZip { get; set; }
        public string MoveDate { get; set; }
        public string MoveType { get; set; }
        public string HomeSize { get; set; }
        public int? OtherRooms { get; set; }
        public LocalMoveQuoteRequest() { }
    }
}
