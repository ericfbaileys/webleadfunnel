﻿using System;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace WebLeadFunnel.Data
{
    public class QlabConsumer
    {
        private readonly string BaseUri = "https://openapi.sirva.com";
        private readonly string clientId = "4eGSORR7hiyHSNWB8EHV3RCt5a0Lcgk4";
        private readonly string clientKey = "7X3GUhoBqSijszt2cZ0iv/iTIJuZyb3D";

        public QlabConsumer()
        {
        }

        /// <summary>
        /// Checks for existing Token or gets a new Token if it has expired
        /// </summary>
        /// <returns>A valid token</returns>
        public async Task<Token> GetAccessToken()
        {
            Token currenToken = null;
            // Check for existing token in the token store
            TokenStore store = TokenStore.Instance;
            if (store.HasTokens)
            {
                currenToken = store.GetCurrentToken();
            }
            // If the token is expired, request a new one
            if (currenToken == null || currenToken.Expiration <= DateTime.Now.AddMinutes(2))
            {
                currenToken = await RequestToken();
                // Add new token to store
                store.StoreNewToken(currenToken);
            }

            return currenToken;
        }

        /// <summary>
        /// A method to request a new OAuth Token
        /// </summary>
        /// <returns>A new valid token</returns>
        private async Task<Token> RequestToken()
        {
            var client = new RestClient(BaseUri);
            var request = new RestRequest("/oauth2/accessrequest", Method.POST);

            // Add options to header
            request.AddHeader("Host", "openapi.sirva.com");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
            request.AddHeader("Authorization", GenerateAuthString);
            request.AddHeader("Accept", "application/json");
            //request.AddHeader("grant_type", "client_credentials");
            request.AddParameter("application/x-www-form-urlencoded", "grant_type=client_credentials", ParameterType.RequestBody);

            var response = await client.ExecuteAsync(request);
            return response.StatusCode == HttpStatusCode.OK ? JsonConvert.DeserializeObject<Token>(response.Content) : null;
        }

        /// <summary>
        /// Property to get an auth string
        /// </summary>
        private string GenerateAuthString => "Basic " + EncodeTo64(clientId + ":" + clientKey);

        /// <summary>
        /// Helper method to encode a string to base64 encoding
        /// </summary>
        /// <param name="toEncode">String to encode</param>
        /// <returns>Base64 encoded string</returns>
        private static string EncodeTo64(string toEncode) => Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(toEncode));
    }
}
