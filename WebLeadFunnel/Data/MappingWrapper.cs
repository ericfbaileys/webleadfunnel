﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BingMapsRESTToolkit;
using GoogleMapsApi;
using GoogleMapsApi.Entities.Directions.Request;
using GoogleMapsApi.Entities.Directions.Response;
using GoogleMapsApi.Entities.Geocoding.Request;
using GoogleMapsApi.Entities.Geocoding.Response;
using Microsoft.ApplicationInsights;
using WebLeadFunnel.Models;
using Route = BingMapsRESTToolkit.Route;

namespace WebLeadFunnel.Data
{
    public class MappingWrapper
    {
        private static readonly string BING_API_KEY = "Atnh_Yoc3rRweYj-phlQxnEOFs9EV0TlZiJAlaZV69K5XcmyVmeuSxJyy9yEmtd3";
        private static readonly string GOOGLE_MAPS_API_KEY = "AIzaSyAsLKTUjTcDtXJhKaTRED1eC_pho419EQk";
        private static TelemetryClient _telemetryClient;

        public MappingWrapper(TelemetryClient telemetryClient)
        {
            _telemetryClient = telemetryClient;
        }

        public async Task<(string City, string State)> GetCityStateForZipCodeAsync(string zipCode)
        {
            var bingGeocodeResponseAsyncOrigin = ServiceManager.GetResponseAsync(new GeocodeRequest()
            {
                BingMapsKey = BING_API_KEY,
                Query = zipCode
            }).GetAwaiter().GetResult();

            string city = string.Empty;
            string stateCode = string.Empty;

            if (bingGeocodeResponseAsyncOrigin?.ResourceSets != null &&
                bingGeocodeResponseAsyncOrigin.ResourceSets.Length > 0 &&
                bingGeocodeResponseAsyncOrigin.ResourceSets[0].Resources != null &&
                bingGeocodeResponseAsyncOrigin.ResourceSets[0].Resources.Length > 0)
            {
                 city = (bingGeocodeResponseAsyncOrigin.ResourceSets[0].Resources[0] as Location)
                    ?.Address.Locality;
                stateCode = (bingGeocodeResponseAsyncOrigin.ResourceSets[0].Resources[0] as Location)
                    ?.Address.AdminDistrict;
            }

            // If either is missing, fall back to Google data
            if (string.IsNullOrEmpty(city) || string.IsNullOrEmpty(stateCode))
            {
                _telemetryClient.TrackEvent("Fell back to Google for Zipcode");
                GeocodingRequest googleGeocodingRequest =
                    new GeocodingRequest
                    {
                        ApiKey = GOOGLE_MAPS_API_KEY,
                        Address = zipCode
                    };
                var googleGeocodeResponse = await GoogleMaps.Geocode.QueryAsync(googleGeocodingRequest);
                if (googleGeocodeResponse.Status == Status.OK)
                {
                    var firstResult = googleGeocodeResponse.Results.First();
                    foreach (var addressComponent in firstResult.AddressComponents)
                    {
                        if (addressComponent.Types.Any(x => x.Equals("locality")))
                        {
                            city = addressComponent.ShortName;
                        }

                        if (addressComponent.Types.Any(x => x.Equals("administrative_area_level_1")))
                        {
                            stateCode = addressComponent.ShortName;
                        }
                    }
                }
            }

            return (city,stateCode);
        }

        public async Task<double> GetDistanceBetweenTwoZipCodes(string originZip, string destinationZip)
        {
            var routeOptions = new RouteOptions
            {
                DistanceUnits = DistanceUnitType.Miles,
                MaxSolutions = 1
            };

            var routeRequest = new RouteRequest
            {
                BingMapsKey = BING_API_KEY,
                Waypoints = new List<SimpleWaypoint>()
                {
                    new SimpleWaypoint(originZip),
                    new SimpleWaypoint(destinationZip)
                },
                RouteOptions = routeOptions
            };


            var bingGeocodeResponseAsyncDistance = await ServiceManager.GetResponseAsync(routeRequest);

            if (bingGeocodeResponseAsyncDistance?.ResourceSets != null &&
                bingGeocodeResponseAsyncDistance.ResourceSets.Length > 0 &&
                bingGeocodeResponseAsyncDistance.ResourceSets[0].Resources != null &&
                bingGeocodeResponseAsyncDistance.ResourceSets[0].Resources.Length > 0)
            {
                Route bingRoute = bingGeocodeResponseAsyncDistance.ResourceSets[0].Resources[0] as Route;
                if (bingRoute.TravelDistance != 0.0)
                {
                    // Try to not send back 0.0
                    return bingRoute.TravelDistance;
                }
            }

            // Try Google for the distance if Bing fails
            DirectionsRequest directionsRequest = new DirectionsRequest()
            {
                ApiKey = GOOGLE_MAPS_API_KEY,
                Origin = originZip,
                Destination = destinationZip,

            };
            var directionsResults = await GoogleMaps.Directions.QueryAsync(directionsRequest);
            Dictionary<string, string> eventDictionary = new Dictionary<string, string>
            {
                { "OriginZip", originZip },
                { "DestinationZip", destinationZip }
            };
            _telemetryClient.TrackEvent("Fell back to Google for Distance", eventDictionary);
            if (directionsResults.Status == DirectionsStatusCodes.OK)
            {
                var directionRoute = directionsResults.Routes.First();
                var directionLegs = directionRoute.Legs.First();
                var distanceMeterValue = directionLegs.Distance.Value;
                return ConvertMetersToMiles(distanceMeterValue);
            }
            // Getting 'Zero Results' means one of the zips doesn't exist
            if (directionsResults.Status == DirectionsStatusCodes.ZERO_RESULTS)
            {
                return -1D;
            }
            return 0.0;
        }

        private static double ConvertMetersToMiles(int meterVal)
        {
            return Math.Round((meterVal / 1609.344), 2);
        }
    }
}
