﻿using System;

namespace WebLeadFunnel.Models
{
    public class VerifyServiceResult
    {
        public string ResponseType { get; set; }
        public string EstimatorName { get; set; }
        public string EstimatorLocation { get; set; }
        public int ScheduleDaysOut { get; set; }
        public DateTime? AppointmentLookupDate { get; set; }

        public VerifyServiceResult()
        {
        }
    }
}
