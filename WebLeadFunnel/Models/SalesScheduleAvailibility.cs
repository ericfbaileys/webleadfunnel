﻿using System.ComponentModel.DataAnnotations;

namespace WebLeadFunnel.Models
{
    public class SalesScheduleAvailibility
    {
        [Key]
        public int Id { get; set; }
        public int DayOfWeek { get; set; }
        public int StartHour24 { get; set; }
        public int EndHour24 { get; set; }
        public SalesScheduleAvailibility()
        {
        }
    }
}
