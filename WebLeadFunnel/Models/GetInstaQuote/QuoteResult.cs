﻿using System;
using Newtonsoft.Json;
using WebLeadFunnel.Data;

namespace WebLeadFunnel.Models.GetInstaQuote
{
    public class QuoteResult
    {
        [JsonProperty("Average")]
        public Average Average { get; set; }

        [JsonProperty("ALot")]
        public ALot ALot { get; set; }

        [JsonProperty("NotMuch")]
        public NotMuch NotMuch { get; set; }

        [JsonProperty("Result")]
        public Result Result { get; set; }
    }
    public class ALot
    {
        private string _AlotHighPrice;
        [JsonProperty("HighPrice")]
        public string HighPrice
        {
            get => decimal.TryParse(_AlotHighPrice, out decimal result) ? decimal.Parse(_AlotHighPrice).AddComissionAndRoundOffto10().ToString() : "NA";
            set => _AlotHighPrice = value;
        }

        private string _AlotLowPrice;
        [JsonProperty("LowPrice")]
        public string LowPrice
        {
            get => decimal.TryParse(_AlotLowPrice, out decimal result) ? decimal.Parse(_AlotLowPrice).AddComissionAndRoundOffto10().ToString() : "N/A";
            set => _AlotLowPrice = value;
        }
    }

    public class Average
    {
        private string _AverageHighPrice;
        [JsonProperty("HighPrice")]
        public string HighPrice
        {
            get => decimal.TryParse(_AverageHighPrice, out decimal result) ? decimal.Parse(_AverageHighPrice).AddComissionAndRoundOffto10().ToString() : "N/A";
            set => _AverageHighPrice = value;
        }

        private string _AverageLowPrice;
        [JsonProperty("LowPrice")]
        public string LowPrice
        {
            get => decimal.TryParse(_AverageLowPrice, out decimal result) ? decimal.Parse(_AverageLowPrice).AddComissionAndRoundOffto10().ToString() : "N/A";
            set => _AverageLowPrice = value;
        }
    }

    public class NotMuch
    {
        private string _NotMuchHighPrice;
        [JsonProperty("HighPrice")]
        public string HighPrice
        {
            get => decimal.TryParse(_NotMuchHighPrice, out decimal result) ? decimal.Parse(_NotMuchHighPrice).AddComissionAndRoundOffto10().ToString() : "N/A";
            set => _NotMuchHighPrice = value;
        }

        private string _NotMuchLowPrice;
        [JsonProperty("LowPrice")]
        public string LowPrice
        {
            get => decimal.TryParse(_NotMuchLowPrice, out decimal result) ? decimal.Parse(_NotMuchLowPrice).AddComissionAndRoundOffto10().ToString() : "N/A";
            set => _NotMuchLowPrice = value;
        }
    }
    public class Result
    {
        [JsonProperty("ResultsMessage")]
        public object ResultsMessage { get; set; }

        [JsonProperty("Results")]
        public string Results { get; set; }

        [JsonProperty("ResultsMessageCount")]
        public long ResultsMessageCount { get; set; }
    }
}
